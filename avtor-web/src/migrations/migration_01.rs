use chrono::Utc;
use deadpool_postgres::Pool;
use log::{debug, error, info};
use tokio_postgres::{Client, Transaction};
use uuid::Uuid;

use crate::{
    common::with_trans_res,
    repo::migrations::{Migration, MigrationCriteria, MigrationRepo, MIGRATION_TABLE},
};

const UP: &'static str = "
create table if not exists accounts (
    id uuid not null primary key,
    name unique varchar(255),
    created_on timestamp default current_timestamp
);
CREATE table if not exists invitations (
    id uuid not null primary key,
    username unique varchar(255) not null,
    password varchar(255) not null,
    roles text not null,
    account_id uuid not null references accounts(id),
    accepted_on timestamp null, 
    created_on timestamp default current_timestamp 
);
CREATE table if not exists users (
    id uuid not null primary key,
    username varchar(255) unique not null,
    password varchar(255) not null,
    roles text not null,
    account_id uuid not null references accounts(id),
    created_on timestamp default current_timestamp 
);

create table if not exists login_attempts (
    id uuid primary key not null,
    ip varchar(255) not null,
    attempted_on timestamp not null
);

create table if not exists blocked_ips (
    id uuid primary key not null,
    ip varchar(255) not null
);

create table if not exists internal_users (
    id uuid primary key not null,
    username varchar(255) unique not null,
    password varchar(255) not null,
    id_active boolean not null
)
";

const DOWN: &'static str = "
drop table internal_users;
drop table login_attempts;
drop table blocked_ips;
drop table invitations;
drop table users;
drop table accounts;";

pub async fn run_migration_up<'a>(client: &Transaction<'a>) -> Result<(), anyhow::Error> {
    let stmt = client.prepare(UP).await?;
    client.execute(&stmt, &[]).await?;
    Ok(())
}

pub async fn run_migrations_down<'a>(client: &Transaction<'a>) -> Result<(), ()> {
    let stmt = client.prepare(DOWN).await.map_err(|_| ())?;
    client.execute(&stmt, &[]).await.map_err(|_| ())?;
    Ok(println!("running migrations down"))
}

pub async fn run_migration(client: &mut Client) -> Result<(), anyhow::Error> {
    let crit = vec![MigrationCriteria::SeqOrderEq(1)];
    let trans_builder = client.build_transaction();
    let trans = trans_builder.start().await?;

    debug!("Checking for migration 1");
    let migration_opt = MigrationRepo::find_by_criteria(&trans)(crit).await?;

    let _: Result<(), anyhow::Error> = match migration_opt {
        Some(_) => {
            debug!("Migration 1 found skipping running migration 1");
            Ok(())
        }
        None => match run_migration_up(&trans).await {
            Err(_) => {
                error!("Migrations 1 Up failed running downs");
                match run_migrations_down(&trans).await {
                    Ok(_) => {
                        info!("Migration 1 down ran without error");
                        Ok(())
                    }
                    Err(_) => {
                        error!("Migration 1 Down failed");
                        error!("This error could be fatal and the database must manually be checked and");
                        error!("put in a consistent state");
                        Ok(())
                    }
                }
            }
            _ => {
                let new_migration = Migration {
                    id: Uuid::new_v4(),
                    name: "migration_01".to_string(),
                    seq_order: 1,
                    up: UP.to_string(),
                    down: DOWN.to_string(),
                    applied_on: Utc::now().naive_utc(),
                };
                MigrationRepo::insert(&trans)(&new_migration)
                    .await
                    .map_err(|e| {
                        error!("Failed to write to the migration table");
                        error!("Please manually check the status of migration 1");
                        e
                    })?;
                info!("Migration 1 ran without error");
                Ok(())
            }
        },
    };
    trans.commit().await.map_err(|e| anyhow::Error::from(e))
}

fn migration_01_record() -> Migration {
    Migration {
        id: Uuid::new_v4(),
        name: "migration_01".to_string(),
        seq_order: 1,
        up: UP.to_string(),
        down: DOWN.to_string(),
        applied_on: Utc::now().naive_utc(),
    }
}

pub async fn run_migrations(pool: Pool) -> Result<(), anyhow::Error> {
    debug!("Running Migration 1");
    with_trans_res(pool, |trans| {
        Box::pin(async move {
            let conds = vec![MigrationCriteria::SeqOrderEq(1)];
            let existing_migration_res = MigrationRepo::find_by_criteria(trans)(conds).await;
            match existing_migration_res {
                Ok(Some(_)) => {
                    info!("Migration 1 found.  Going to next migration");
                    Ok(())
                }
                Ok(None) => {
                    info!("Running Migration 1");

                    #[allow(unused_results)]
                    run_migration_up(trans).await;

                    info!("Inserting Migration Record");

                    #[allow(unused_results)]
                    MigrationRepo::insert(trans)(&migration_01_record()).await;

                    info!("Migration 1 completed Ok");
                    Ok(())
                }
                Err(e) => Err(anyhow::Error::from(e)),
            }
        })
    })
    .await
    .and_then(|r| r)
}
