extern crate proc_macro;

macro_rules! view_model {
    (
        $(#[$struct_meta:meta])*
        pub struct $name:ident {
            $(
                $(#[$field_meta:meta])*
                $field_vis:vis $field_name:ident : $field_type:ty
            ),*$(,)+
        }
    ) => {

        $(#[$struct_meta])*
        pub struct $name {
            $(
                $(#[$field_meta])*
                pub $field_name : $field_type,
            )*
        }

        paste::paste! {
            #[derive(Debug)]
            pub enum [<$name Fields>] {
                $([<$field_name:camel>]),*
            }

            impl std::fmt::Display for [<$name Fields>] {
                fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
                    std::fmt::Debug::fmt(self, f)
                }
            }
        }



        paste::paste! {
            #[derive(Debug,Clone,serde::Serialize,serde::Deserialize)]
            pub enum [<$name Criteria>] {
                $([<$field_name:camel Eq>]($field_type)),*,
                $([<$field_name:camel Neq >]($field_type)),*,
                $([<$field_name:camel Gt>]($field_type)),*,
                $([<$field_name:camel Gte>]($field_type)),*,
                $([<$field_name:camel Lt>]($field_type)),*,
                $([<$field_name:camel Lte>]($field_type)),*,
                $([<$field_name:camel In>](Vec<$field_type>)),*,
                $([<$field_name:camel Nin>](Vec<$field_type>)),*,
                $([<$field_name:camel Like>]($field_type)),*,
                $([<$field_name:camel NLike>]($field_type)),*,
                $([<$field_name:camel FuzzySearch>]($field_type)),*,
            }

            impl [<$name Criteria>] {
                pub fn to_query_condition<'a>(&'a self, map: std::collections::HashMap<&'static str, &'static str>) -> QueryCondition<'a> {
                    match self {
                        $([<$name Criteria>]::[<$field_name:camel Eq>](x) => QueryCondition::Eq(map.get(stringify!($field_name)).unwrap(), x)),*,
                        $([<$name Criteria>]::[<$field_name:camel Neq>](x) => QueryCondition::Neq(map.get(stringify!($field_name)).unwrap(), x)),*,
                        $([<$name Criteria>]::[<$field_name:camel Gt>](x) => QueryCondition::Gt(map.get(stringify!($field_name)).unwrap(), x)),*,
                        $([<$name Criteria>]::[<$field_name:camel Gte>](x) => QueryCondition::Gte(map.get(stringify!($field_name)).unwrap(), x)),*,
                        $([<$name Criteria>]::[<$field_name:camel Lt>](x) => QueryCondition::Lt(map.get(stringify!($field_name)).unwrap(), x)),*,
                        $([<$name Criteria>]::[<$field_name:camel Lte>](x) => QueryCondition::Lte(map.get(stringify!($field_name)).unwrap(), x)),*,
                        $([<$name Criteria>]::[<$field_name:camel In>](x) => QueryCondition::In(map.get(stringify!($field_name)).unwrap(), x)),*,
                        $([<$name Criteria>]::[<$field_name:camel Nin>](x) => QueryCondition::Nin(map.get(stringify!($field_name)).unwrap(), x)),*,
                        $([<$name Criteria>]::[<$field_name:camel Like>](x) => QueryCondition::Like(map.get(stringify!($field_name)).unwrap(), x)),*,
                        $([<$name Criteria>]::[<$field_name:camel NLike>](x) => QueryCondition::NLike(map.get(stringify!($field_name)).unwrap(), x)),*,
                        $([<$name Criteria>]::[<$field_name:camel FuzzySearch>](x) => QueryCondition::FuzzySearch(map.get(stringify!($field_name)).unwrap(), x)),*,
                    }
                }
            }
        }

        paste::paste! {
            #[derive(Debug,Clone,serde::Serialize,serde::Deserialize)]
            pub struct [<$name CriteriaStruct>]{
                pub $([<$field_name _eq>]: Option<$field_type>),*,
                pub $([<$field_name _neq >]: Option<$field_type>),*,
                pub $([<$field_name _gt>]: Option<$field_type>),*,
                pub $([<$field_name _gte>]: Option<$field_type>),*,
                pub $([<$field_name _lt>]: Option<$field_type>),*,
                pub $([<$field_name _lte>]: Option<$field_type>),*,
                pub $([<$field_name _in>]: Option<Vec<$field_type>>),*,
                pub $([<$field_name _nin>]: Option<Vec<$field_type>>),*,
                pub $([<$field_name _like>]: Option<$field_type>),*,
                pub $([<$field_name _nlike>]: Option<$field_type>),*,
                pub $([<$field_name _fuzzy_search>]: Option<$field_type>),*,
                pub limit: u32,
                pub page: u32,
            }

            impl [<$name CriteriaStruct>] {
                pub fn to_criteria(self) -> Vec<[<$name Criteria>]> {
                    let mut c = vec![];
                    $(if let Some(x) = self.[<$field_name _eq>] {
                        c.push([<$name Criteria>]::[<$field_name:camel Eq>](x));
                    })*
                    $(if let Some(x) = self.[<$field_name _neq>] {
                        c.push([<$name Criteria>]::[<$field_name:camel Neq>](x));
                    })*
                    $(if let Some(x) = self.[<$field_name _gt>] {
                        c.push([<$name Criteria>]::[<$field_name:camel Gt>](x));
                    })*
                    $(if let Some(x) = self.[<$field_name _gte>] {
                        c.push([<$name Criteria>]::[<$field_name:camel Gte>](x));
                    })*
                    $(if let Some(x) = self.[<$field_name _lt>] {
                        c.push([<$name Criteria>]::[<$field_name:camel Lt>](x));
                    })*
                    $(if let Some(x) = self.[<$field_name _lte>] {
                        c.push([<$name Criteria>]::[<$field_name:camel Lte>](x));
                    })*
                    $(if let Some(x) = self.[<$field_name _in>] {
                        if !x.is_empty() {
                            c.push([<$name Criteria>]::[<$field_name:camel In>](x));
                        }
                    })*
                    $(if let Some(x) = self.[<$field_name _nin>] {
                        if !x.is_empty() {
                            c.push([<$name Criteria>]::[<$field_name:camel Nin>](x));
                        }
                    })*
                    $(if let Some(x) = self.[<$field_name _like>] {
                        c.push([<$name Criteria>]::[<$field_name:camel Like>](x));
                    })*
                    $(if let Some(x) = self.[<$field_name _nlike>] {
                        c.push([<$name Criteria>]::[<$field_name:camel NLike>](x));
                    })*
                    $(if let Some(x) = self.[<$field_name _fuzzy_search>] {
                        c.push([<$name Criteria>]::[<$field_name:camel FuzzySearch>](x));
                    })*
                    c
                }
            }
        }
    };
}

pub(crate) use view_model;
