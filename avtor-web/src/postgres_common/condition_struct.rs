extern crate proc_macro;

macro_rules!  condition_struct {
    (
        $(#[$struct_meta:meta])*
        pub struct $name:ident {
            $(
                $(#[$field_meta:meta])*
                $field_vis:vis $field_name:ident : $field_type:ty
            ),*$(,)+
        }
    ) => {
        paste::paste! {
            #[derive(Default,Debug,Clone,serde::Serialize,serde::Deserialize)]
            pub struct [<$name ConditionStruct>] {
                pub $([<$field_name _eq>]: Option<$field_type>),*,
                pub $([<$field_name _neq >]: Option<$field_type>),*,
                pub $([<$field_name _gt>]: Option<$field_type>),*,
                pub $([<$field_name _gte>]: Option<$field_type>),*,
                pub $([<$field_name _lt>]: Option<$field_type>),*,
                pub $([<$field_name _lte>]: Option<$field_type>),*,
                pub $([<$field_name _in>]: Option<Vec<$field_type>>),*,
                pub $([<$field_name _nin>]: Option<Vec<$field_type>>),*,
                pub $([<$field_name _like>]: Option<$field_type>),*,
                pub $([<$field_name _nlike>]: Option<$field_type>),*,
                pub $([<$field_name _fuzzy_search>]: Option<$field_type>),*,
                pub limit: u32,
                pub page: u32,
            }
        }
    }
}

condition_struct! {
    #[derive(Serialize)]
    pub struct Item {
        pub id: String,
        pub name: String,
        pub other: Option<String>,
    }
}
