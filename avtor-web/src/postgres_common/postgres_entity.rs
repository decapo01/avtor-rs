extern crate proc_macro;

macro_rules! entity {
    (
        $tbl:expr,
        $(#[$struct_meta:meta])*
        pub struct $name:ident {
            $(
                $(#[$field_meta:meta])*
                $field_vis:vis $field_name:ident : $field_type:ty
            ),*$(,)+
        }) => {



        $(#[$struct_meta])*
        pub struct $name {
            $(
                $(#[$field_meta])*
                pub $field_name : $field_type,
            )*
        }

        paste::paste! {
            #[derive(Debug)]
            pub enum [<$name Fields>] {
                $([<$field_name:camel>]),*
            }

            impl std::fmt::Display for [<$name Fields>] {
                fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
                    std::fmt::Debug::fmt(self, f)
                }
            }
        }

        paste::paste! {
            #[derive(Debug,Clone,serde::Serialize,serde::Deserialize)]
            pub enum [<$name Criteria>] {
                $([<$field_name:camel Eq>]($field_type)),*,
                $([<$field_name:camel Neq >]($field_type)),*,
                $([<$field_name:camel Gt>]($field_type)),*,
                $([<$field_name:camel Gte>]($field_type)),*,
                $([<$field_name:camel Lt>]($field_type)),*,
                $([<$field_name:camel Lte>]($field_type)),*,
                $([<$field_name:camel In>](Vec<$field_type>)),*,
                $([<$field_name:camel Nin>](Vec<$field_type>)),*,
                $([<$field_name:camel Like>]($field_type)),*,
                $([<$field_name:camel NLike>]($field_type)),*,
                $([<$field_name:camel FuzzySearch>]($field_type)),*,
            }


            #[derive(Default,Debug,Clone,serde::Serialize,serde::Deserialize)]
            pub struct [<$name CriteriaStruct>] {
                pub $([<$field_name _eq>]: Option<$field_type>),*,
                pub $([<$field_name _neq >]: Option<$field_type>),*,
                pub $([<$field_name _gt>]: Option<$field_type>),*,
                pub $([<$field_name _gte>]: Option<$field_type>),*,
                pub $([<$field_name _lt>]: Option<$field_type>),*,
                pub $([<$field_name _lte>]: Option<$field_type>),*,
                pub $([<$field_name _in>]: Option<Vec<$field_type>>),*,
                pub $([<$field_name _nin>]: Option<Vec<$field_type>>),*,
                pub $([<$field_name _like>]: Option<$field_type>),*,
                pub $([<$field_name _nlike>]: Option<$field_type>),*,
                pub $([<$field_name _fuzzy_search>]: Option<$field_type>),*,
                pub limit: u32,
                pub page: u32,
            }


            impl [<$name Criteria>] {
                pub fn to_query_condition<'a>(&'a self) -> QueryCondition<'a> {
                    match self {
                        $([<$name Criteria>]::[<$field_name:camel Eq>](x) => QueryCondition::Eq(stringify!($field_name), x)),*,
                        $([<$name Criteria>]::[<$field_name:camel Neq>](x) => QueryCondition::Neq(stringify!($field_name), x)),*,
                        $([<$name Criteria>]::[<$field_name:camel Gt>](x) => QueryCondition::Gt(stringify!($field_name), x)),*,
                        $([<$name Criteria>]::[<$field_name:camel Gte>](x) => QueryCondition::Gte(stringify!($field_name), x)),*,
                        $([<$name Criteria>]::[<$field_name:camel Lt>](x) => QueryCondition::Lt(stringify!($field_name), x)),*,
                        $([<$name Criteria>]::[<$field_name:camel Lte>](x) => QueryCondition::Lte(stringify!($field_name), x)),*,
                        $([<$name Criteria>]::[<$field_name:camel In>](x) => QueryCondition::In(stringify!($field_name), x)),*,
                        $([<$name Criteria>]::[<$field_name:camel Nin>](x) => QueryCondition::Nin(stringify!($field_name), x)),*,
                        $([<$name Criteria>]::[<$field_name:camel Like>](x) => QueryCondition::Like(stringify!($field_name), x)),*,
                        $([<$name Criteria>]::[<$field_name:camel NLike>](x) => QueryCondition::NLike(stringify!($field_name), x)),*,
                        $([<$name Criteria>]::[<$field_name:camel FuzzySearch>](x) => QueryCondition::FuzzySearch(stringify!($field_name), x)),*,
                    }
                }
            }

            impl [<$name CriteriaStruct>] {
                pub fn to_criteria(self) -> Vec<[<$name Criteria>]> {
                    let mut c = vec![];
                    $(if let Some(x) = self.[<$field_name _eq>] {
                        c.push([<$name Criteria>]::[<$field_name:camel Eq>](x));
                    })*
                    $(if let Some(x) = self.[<$field_name _neq>] {
                        c.push([<$name Criteria>]::[<$field_name:camel Neq>](x));
                    })*
                    $(if let Some(x) = self.[<$field_name _gt>] {
                        c.push([<$name Criteria>]::[<$field_name:camel Gt>](x));
                    })*
                    $(if let Some(x) = self.[<$field_name _gte>] {
                        c.push([<$name Criteria>]::[<$field_name:camel Gte>](x));
                    })*
                    $(if let Some(x) = self.[<$field_name _lt>] {
                        c.push([<$name Criteria>]::[<$field_name:camel Lt>](x));
                    })*
                    $(if let Some(x) = self.[<$field_name _lte>] {
                        c.push([<$name Criteria>]::[<$field_name:camel Lte>](x));
                    })*
                    $(if let Some(x) = self.[<$field_name _in>] {
                        if !x.is_empty() {
                            c.push([<$name Criteria>]::[<$field_name:camel In>](x));
                        }
                    })*
                    $(if let Some(x) = self.[<$field_name _nin>] {
                        if !x.is_empty() {
                            c.push([<$name Criteria>]::[<$field_name:camel Nin>](x));
                        }
                    })*
                    $(if let Some(x) = self.[<$field_name _like>] {
                        c.push([<$name Criteria>]::[<$field_name:camel Like>](x));
                    })*
                    $(if let Some(x) = self.[<$field_name _nlike>] {
                        c.push([<$name Criteria>]::[<$field_name:camel NLike>](x));
                    })*
                    $(if let Some(x) = self.[<$field_name _fuzzy_search>] {
                        c.push([<$name Criteria>]::[<$field_name:camel FuzzySearch>](x));
                    })*
                    c
                }
            }
        }


        impl $name {

            pub fn field_names() -> &'static [&'static str] {
                static NAMES: &'static [&'static str] = &[$(stringify!($field_name)),*];
                NAMES
            }

            pub fn field_name_set() -> HashSet<String> {
                let names: HashSet<String> = HashSet::from_iter([$(stringify!($field_name).to_string()),*]);
                names
            }

            pub fn field_types() -> &'static [&'static str] {
                static TYPES: &'static [&'static str] = &[$(stringify!($field_type)),*];
                TYPES
            }

            pub fn from_row(row: tokio_postgres::Row) -> $name {
                $(let $field_name: $field_type = row.get(stringify!($field_name));)*
                $name {
                    $($field_name),*
                }
            }

            pub fn to_params_x<'a>(&'a self) -> Vec<&'a (dyn tokio_postgres::types::ToSql + Sync)> {
                vec![
                    $(&self.$field_name as &(dyn tokio_postgres::types::ToSql + Sync)),*
                ][1..].into_iter().map(|x| *x as &(dyn tokio_postgres::types::ToSql + Sync)).collect::<Vec<&'a (dyn tokio_postgres::types::ToSql + Sync)>>()
            }
        }

        paste::paste! {
            pub struct [<$name Repo>] {}
            impl [<$name Repo>] {

                pub fn insert<'a>(trans: &'a Transaction<'a>) -> impl FnOnce(&'a $name) -> BoxFuture<'a, Result<(), anyhow::Error>> {
                    move |item| {
                        Box::pin(async move {
                            let fields = field_names_without_id($name::field_names());
                            insert(
                                trans,
                                $tbl,
                                "id",
                                fields.as_slice(),
                                &item.id,
                                item.to_params_x().as_slice()
                            )
                            .await
                        })
                    }
                }

                pub fn update<'a>(trans: &'a Transaction) -> impl FnOnce(&'a $name) -> BoxFuture<'a, Result<(), anyhow::Error>> {
                    move |item| {
                        Box::pin(async move {
                            let fields = field_names_without_id($name::field_names());
                            update(
                                trans,
                                $tbl,
                                "id",
                                fields.as_slice(),
                                &item.id,
                                &item.to_params_x()
                            )
                            .await
                            .map_err(|e| anyhow!(e.to_string()))
                        })
                    }
                }

                pub fn delete<'a>(trans: &'a Transaction) -> impl FnOnce(&'a $name) -> BoxFuture<'a, Result<(), anyhow::Error>> {
                    move |item| {
                        Box::pin(async move {
                            delete(trans, $tbl, &item.id).await
                        })
                    }
                }

                pub fn find_by_criteria<'a>(trans: &'a Transaction) -> impl FnOnce(Vec<[<$name Criteria>]>) -> BoxFuture<'a, Result<Option<$name>, RepoError>> {
                    move |crit: Vec<[<$name Criteria>]>| {
                        Box::pin(async move {
                            let cond = crit.iter().map(|x| x.to_query_condition()).collect();
                            select(&trans, $tbl, &cond, $name::from_row).await.map_err(|e| RepoError::RepoError(e.to_string()))
                        })
                    }
                }

                pub fn find_all_by_criteria<'a>(trans: &'a Transaction) -> impl FnOnce(Vec<[<$name Criteria>]>, Option<SortCond>) -> BoxFuture<'a, Result<Vec<$name>, RepoError>> {
                    move |crit: Vec<[<$name Criteria>]>, sort: Option<SortCond>| {
                        Box::pin(async move {
                            let cond = crit.iter().map(|x| x.to_query_condition()).collect();
                            select_all(&trans, $tbl, &cond, &sort, $name::from_row).await.map_err(|e| RepoError::RepoError(e.to_string()))
                        })
                    }
                }

                pub fn find_all_by_required_criteria<'a>(trans: &'a Transaction, table: &'static str) -> impl FnOnce(Vec<[<$name Criteria>]>, Option<SortCond>) -> BoxFuture<'a, Result<Vec<$name>, anyhow::Error>> {
                    move |crit: Vec<[<$name Criteria>]>, sort: Option<SortCond>| {
                        Box::pin(async move {
                            if crit.is_empty() {
                                Ok(vec![])
                            } else {
                                let cond = crit.iter().map(|x| x.to_query_condition()).collect();
                                select_all(&trans, table, &cond, &sort, $name::from_row).await.map_err(|e| anyhow!("RepoErr {}", e))
                            }
                        })
                    }
                }

              // tech_debt: will need to complete this or make a generic function
              // pub fn find_stream_by_criteria<'a>(trans: &'a Transaction, table: &'a String) -> impl FnOnce(Vec<[<$name Criteria>]>) -> BoxFuture<'a, Result<BoxStream<'a, $name>, RepoError>> {
              //     move |crit: Vec<[<$name Criteria>]>| {
              //         Box::pin(async move {
              //             let cond = crit.iter().map(|x| x.to_query_condition()).collect();
              //             let r = select_raw(trans, table, &cond, $name::from_row).await;
              //             match r {
              //                 Ok(s) => Ok(s.boxed()),
              //                 Err(e) => Err(e)
              //             }
              //         })
              //     }
              // }
            }
        }
    }
}

pub(crate) use entity;
