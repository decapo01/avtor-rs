use std::{
    fmt::{Debug, Display},
    hash::Hash,
    ops::Deref,
};

use actix_web::web::Query;
use anyhow::{Error, Ok};
use futures::{pin_mut, Future, Stream, StreamExt, TryStreamExt};
use postgres_types::{FromSql, ToSql};
use std::{collections::HashMap, vec};
use std::fmt::format;
use tokio_postgres::{Client, Row, RowStream, Statement, Transaction};
use uuid::Uuid;

#[derive(thiserror::Error)]
pub enum RepoError {
    #[error("repo error {0}")]
    RepoError(String),
}

impl Debug for RepoError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            RepoError::RepoError(estr) => f.debug_tuple("RepoError").field(estr).finish(),
        }
    }
}

pub fn create_insert_sql(table: &'static str, id_field: &'static str, fields: &[String]) -> String {
    let fields_sql: String = fields
        .iter()
        .fold(id_field.to_string(), |acc, x| format!("{acc}, {x}"));
    let field_range = 2..fields.len() + 2;
    let field_params = field_range.fold("$1".to_string(), |acc, x| format!("{acc}, ${x}"));
    format!("insert into {table} ({fields_sql}) values ({field_params})")
}

pub fn create_update_sql(table: &'static str, id_field: &'static str, fields: &[String]) -> String {
    let (head, tail) = fields.split_at(1);
    let first = head.first().unwrap();
    let first = format!("{first} = $1");
    let (fields_sql, _) = tail.into_iter().fold((first, 2), |acc, x| {
        let (q, i) = acc;
        (format!("{q} , {x} = ${i}"), i + 1)
    });
    let id_field_idx = fields.len() + 1;
    format!("update {table} set {fields_sql} where {id_field} = ${id_field_idx}",)
}

pub async fn insert<'a>(
    client: &Transaction<'a>,
    table: &'static str,
    id_field: &'static str,
    fields: &[String],
    id_param: &(dyn ToSql + Sync),
    params: &[&(dyn ToSql + Sync)],
) -> Result<(), Error> {
    let insert_sql = create_insert_sql(table, id_field, fields);
    let stmt = client.prepare(&insert_sql).await?;
    let all_params = &[&[id_param], params].concat();
    client.execute(&stmt, all_params.as_slice()).await?;
    Ok(())
}

pub async fn update<'a>(
    client: &Transaction<'a>,
    table: &'static str,
    id_field: &'static str,
    fields: &[String],
    id_param: &(dyn ToSql + Sync),
    params: &[&(dyn ToSql + Sync)],
) -> Result<(), Error> {
    let update_sql = create_update_sql(table, id_field, fields);
    let stmt = client.prepare(&update_sql).await?;
    let all_params = &[params, &[id_param]].concat();
    client.execute(&stmt, all_params.as_slice()).await?;
    Ok(())
}

pub fn repo_error_from_error(e: tokio_postgres::Error) -> RepoError {
    RepoError::RepoError(e.to_string())
}

pub async fn delete<'a>(
    trans: &'a Transaction<'a>,
    table: &'static str,
    id_param: &'a (dyn ToSql + Sync),
) -> Result<(), Error> {
    let sql = format!("delete * from {table} where id = $1");
    let stmt = trans.prepare(&sql).await.map_err(repo_error_from_error)?;
    trans
        .execute(&stmt, &[id_param])
        .await
        .map_err(repo_error_from_error)?;
    Ok(())
}

trait ValueTrait: ToSql + Display {}

pub type Field = &'static str;
pub type Value = (dyn ToSql + Sync);

#[derive(Clone)]
pub enum QueryCondition<'a> {
    Eq(Field, &'a Value),
    Neq(Field, &'a Value),
    Gt(Field, &'a Value),
    Gte(Field, &'a Value),
    Lt(Field, &'a Value),
    Lte(Field, &'a Value),
    In(Field, &'a Value),
    Nin(Field, &'a Value),
    Like(Field, &'a Value),
    NLike(Field, &'a Value),
    FuzzySearch(Field, &'a Value),
    NoOp,
}

#[derive(Clone)]
pub enum QueryCondition2<V: ToSql + Sync + Clone> {
    Eq(Field, V),
    Neq(Field, V),
    Gt(Field, V),
    Gte(Field, V),
    Lt(Field, V),
    Lte(Field, V),
    In(Field, V),
    Nin(Field, V),
    Like(Field, V),
    NLike(Field, V),
    FuzzySearch(Field, V),
}

pub enum QueryCondition3 {
    Eq(Field, Box<(dyn ToSql + Sync)>),
    // Neq(Field, dyn ToSql + Sync),
    // Gt(Field, dyn ToSql + Sync),
    // Gte(Field, dyn ToSql + Sync),
    // Lt(Field, dyn ToSql + Sync),
    // Lte(Field, dyn ToSql + Sync),
    // In(Field, dyn ToSql + Sync),
    // Nin(Field, dyn ToSql + Sync),
    // Like(Field, dyn ToSql + Sync),
    // NLike(Field, dyn ToSql + Sync),
    // FuzzySearch(Field, dyn ToSql + Sync),
}

fn query_cond_to_item(q_cond_3: QueryCondition3) -> Box<(dyn ToSql + Sync)> {
    match q_cond_3 {
        QueryCondition3::Eq(_, x) => x,
        // QueryCondition3::Neq(_, x) => x,
        // QueryCondition3::Gt(_, x) => x,
        // QueryCondition3::Gte(_, x) => x,
        // QueryCondition3::Lt(_, x) => x,
        // QueryCondition3::Lte(_, x) => x,
        // QueryCondition3::In(_, x) => x,
        // QueryCondition3::Nin(_, x) => x,
        // QueryCondition3::Like(_, x) => x,
        // QueryCondition3::NLike(_, x) => x,
        // QueryCondition3::FuzzySearch(_, x) => x
    }
}

pub fn query_cond_to_string(q_cond: &QueryCondition, field_idx: u16) -> String {
    match q_cond {
        QueryCondition::Eq(field, _) => format!("{field} = ${field_idx}"),
        QueryCondition::Neq(field, _) => format!("{field} != ${field_idx}"),
        QueryCondition::Gt(field, _) => format!("{field} > ${field_idx}"),
        QueryCondition::Gte(field, _) => format!("{field} >= ${field_idx}"),
        QueryCondition::Lt(field, _) => format!("{field} <= ${field_idx}"),
        QueryCondition::Lte(field, _) => format!("{field} <= ${field_idx}"),
        QueryCondition::In(field, _) => format!("{field} = Any(${field_idx})"),
        QueryCondition::Nin(field, _) => format!("{field} != Any(${field_idx})"),
        QueryCondition::Like(field, _) => format!("{field} like ${field_idx}"),
        QueryCondition::NLike(field, _) => format!("{field} not like ${field_idx}"),
        QueryCondition::FuzzySearch(field, _) => format!("{field} % ${field_idx}"),
        QueryCondition::NoOp => "".to_string(),
    }
}

pub fn query_cond_to_string_2<V: ToSql + Sync + Clone>(
    q_cond: QueryCondition2<V>,
    field_idx: u16,
) -> String {
    match q_cond {
        QueryCondition2::Eq(field, _) => format!("{field} = ${field_idx}"),
        QueryCondition2::Neq(field, _) => format!("{field} != ${field_idx}"),
        QueryCondition2::Gt(field, _) => format!("{field} > ${field_idx}"),
        QueryCondition2::Gte(field, _) => format!("{field} >= ${field_idx}"),
        QueryCondition2::Lt(field, _) => format!("{field} <= ${field_idx}"),
        QueryCondition2::Lte(field, _) => format!("{field} <= ${field_idx}"),
        QueryCondition2::In(field, _) => format!("{field} = Any(${field_idx})"),
        QueryCondition2::Nin(field, _) => format!("{field} != Any(${field_idx})"),
        QueryCondition2::Like(field, _) => format!("{field} like ${field_idx}"),
        QueryCondition2::NLike(field, _) => format!("{field} not like ${field_idx}"),
        QueryCondition2::FuzzySearch(field, _) => format!("{field} % ${field_idx}"),
    }
}

type ValueTwo = Box<dyn ToSql + Sync>;

pub enum QueryCond {
    Eq(Field, ValueTwo),
    Neq(Field, ValueTwo),
    Gt(Field, ValueTwo),
    Gte(Field, ValueTwo),
    Lt(Field, ValueTwo),
    Lte(Field, ValueTwo),
    In(Field, ValueTwo),
    Nin(Field, ValueTwo),
    Like(Field, ValueTwo),
    NLike(Field, ValueTwo),
    FuzzySearch(Field, ValueTwo),
}

pub fn query_cond_to_string_(q_cond: &QueryCond, n: i32) -> String {
    match q_cond {
        QueryCond::Eq(f, _) => format!("{} = ${}", f, n.to_string()),
        QueryCond::Neq(f, _) => format!("{} != ${}", f, n.to_string()),
        QueryCond::Gt(f, _) => format!("{} > ${}", f, n.to_string()),
        QueryCond::Gte(f, _) => format!("{} >= ${}", f, n.to_string()),
        QueryCond::Lt(f, _) => format!("{} <= ${}", f, n.to_string()),
        QueryCond::Lte(f, _) => format!("{} <= ${}", f, n.to_string()),
        QueryCond::In(f, _) => format!("{} = Any(${})", f, n.to_string()),
        QueryCond::Nin(f, _) => format!("{} != Any(${})", f, n.to_string()),
        QueryCond::Like(f, _) => format!("{} like ${}", f, n.to_string()),
        QueryCond::NLike(f, _) => format!("{} not like ${}", f, n.to_string()),
        QueryCond::FuzzySearch(f, _) => format!("{} % ${}", f, n.to_string()),
    }
}

pub fn query_cond_to_val(item: QueryCond) -> ValueTwo {
    match item {
        QueryCond::Eq(_, x) => x,
        _ => Box::new(true)
    }
}

pub fn query_cond_to_vals(items: Vec<QueryCond>) -> Vec<ValueTwo> {
    items
        .into_iter()
        .map(query_cond_to_val)
        .collect()
}

pub async fn quick_func(client: &Client, items: Vec<ValueTwo>) -> Vec<Row> {
    let stmt : Statement = client.prepare("").await.unwrap();
    // let params: Vec<&(dyn ToSql + Sync)> = items.iter().map(|a| &**a).collect();
    let params: Vec<&(dyn ToSql + Sync)> =
        items
            .iter()
            .map(|a| a.as_ref())
            .collect();
    let rows: Vec<Row> = client.query(&stmt, params.as_slice()).await.unwrap();
    rows
}

pub fn extract_value<'a>(query_cond: &'a QueryCondition<'a>) -> &'a Value {
    match query_cond {
        QueryCondition::Eq(_, x) => *x,
        QueryCondition::Neq(_, x) => *x,
        QueryCondition::Gt(_, x) => *x,
        QueryCondition::Gte(_, x) => *x,
        QueryCondition::Lt(_, x) => *x,
        QueryCondition::Lte(_, x) => *x,
        QueryCondition::In(_, x) => *x,
        QueryCondition::Nin(_, x) => *x,
        QueryCondition::Like(_, x) => *x,
        QueryCondition::NLike(_, x) => *x,
        QueryCondition::FuzzySearch(_, x) => *x,
        QueryCondition::NoOp => &true,
    }
}

pub fn foo(x: &Option<String>) -> String {
    match x {
        Some(y) => y.clone(),
        None => "bar".to_string(),
    }
}

/*
async fn bar(client: Client, params: Vec<Box<(dyn ToSql + Sync)>>) -> () {
    let stmt = client.prepare("").await.unwrap();
    let mut ps = vec![];
    for x in params {
        ps.push(*x);
    }
    let r = client.query(&stmt, ps.as_slice()).await.unwrap();
    ()
}
*/
#[derive(Debug, Clone)]
pub enum SortCond {
    Asc(String),
    Dsc(String),
}

pub fn sort_from_sort_cond(s_cond: &SortCond) -> String {
    match s_cond {
        SortCond::Asc(i) => format!("order by {} asc", i),
        SortCond::Dsc(i) => format!("order by {} desc", i),
    }
}

trait ToUrlValue {
    fn to_url_value(&self) -> String;
}

impl ToUrlValue for String {
    fn to_url_value(&self) -> String {
        self.clone()
    }
}

impl ToUrlValue for uuid::Uuid {
    fn to_url_value(&self) -> String {
        self.to_string()
    }
}

impl<T> ToUrlValue for Option<T>
where
    T: ToUrlValue,
{
    fn to_url_value(&self) -> String {
        match self {
            Some(x) => x.to_url_value(),
            None => "none".to_string(),
        }
    }
}

pub fn url_string_from_hash_map(hash_map: HashMap<String, String>) -> String {
    hash_map.into_iter().fold("".to_string(), |acc, (k, v)| {
        if acc == "".to_string() {
            format!("{}={}", k, v)
        } else {
            format!("{}&{}={}", acc, k, v)
        }
    })
}

type UrlValue = dyn Display;

pub enum UrlQueryCondition<'a> {
    Eq(Field, &'a UrlValue),
    Neq(Field, &'a UrlValue),
    Gt(Field, &'a UrlValue),
    Gte(Field, &'a UrlValue),
    Lt(Field, &'a UrlValue),
    Lte(Field, &'a UrlValue),
    In(Field, &'a UrlValue),
    Nin(Field, &'a UrlValue),
    Like(Field, &'a UrlValue),
    NLike(Field, &'a UrlValue),
    FuzzySearch(Field, &'a UrlValue),
}

pub fn query_condition_to_url(q_cond: &UrlQueryCondition) -> String {
    match q_cond {
        UrlQueryCondition::Eq(f, value) => format!("{}_eq={}", f, value.to_string()),
        UrlQueryCondition::Neq(f, value) => format!("{}_neq={}", f, value.to_string()),
        UrlQueryCondition::Gt(f, value) => format!("{}_gt={}", f, value.to_string()),
        UrlQueryCondition::Gte(f, value) => format!("{}_gte={}", f, value.to_string()),
        UrlQueryCondition::Lt(f, value) => format!("{}_lt={}", f, value.to_string()),
        UrlQueryCondition::Lte(f, value) => format!("{}_lte={}", f, value.to_string()),
        UrlQueryCondition::In(f, value) => format!("{}_in={}", f, value.to_string()),
        UrlQueryCondition::Nin(f, value) => format!("{}_nin={}", f, value.to_string()),
        UrlQueryCondition::Like(f, value) => format!("{}_like={}", f, value.to_string()),
        UrlQueryCondition::NLike(f, value) => format!("{}_nlike={}", f, value.to_string()),
        UrlQueryCondition::FuzzySearch(f, value) => {
            format!("{}_fuzzy_search={}", f, value.to_string())
        }
    }
}

trait NewTrait: ToSql + Sized + Sync {}

pub fn params_from_query_conds<'a>(
    query_conds: &'a Vec<QueryCondition<'a>>,
) -> Vec<&'a (dyn ToSql + Sync)> {
    query_conds
        .into_iter()
        .map(|x| match x {
            QueryCondition::Eq(_, p) => *p,
            QueryCondition::Neq(_, p) => *p,
            QueryCondition::Gt(_, p) => *p,
            QueryCondition::Gte(_, p) => *p,
            QueryCondition::Lt(_, p) => *p,
            QueryCondition::Lte(_, p) => *p,
            QueryCondition::In(_, p) => *p,
            QueryCondition::Nin(_, p) => *p,
            QueryCondition::Like(_, p) => *p,
            QueryCondition::NLike(_, p) => *p,
            QueryCondition::FuzzySearch(_, p) => *p,
            QueryCondition::NoOp => &true,
        })
        .collect()
}

pub fn params_from_query_conds_2<V: ToSql + Sync + Clone>(
    query_conds: Vec<QueryCondition2<V>>,
) -> Vec<V> {
    query_conds
        .into_iter()
        .map(|x| match x {
            QueryCondition2::Eq(_, p) => p,
            QueryCondition2::Neq(_, p) => p,
            QueryCondition2::Gt(_, p) => p,
            QueryCondition2::Gte(_, p) => p,
            QueryCondition2::Lt(_, p) => p,
            QueryCondition2::Lte(_, p) => p,
            QueryCondition2::In(_, p) => p,
            QueryCondition2::Nin(_, p) => p,
            QueryCondition2::Like(_, p) => p,
            QueryCondition2::NLike(_, p) => p,
            QueryCondition2::FuzzySearch(_, p) => p,
        })
        .collect()
}

pub fn where_string_from_query_conds_2<V: ToSql + Sync + Copy>(
    query_conds: Vec<QueryCondition2<V>>,
) -> String {
    let (where_part, _) = query_conds.into_iter().fold(("".to_string(), 1), |acc, x| {
        let (q, i) = acc;
        (format!("{} and {}", q, query_cond_to_string_2(x, i)), i + 1)
    });
    where_part
}

pub fn where_string_from_query_conds<'a>(query_conds: &'a Vec<QueryCondition<'a>>) -> String {
    let (where_part, _) = query_conds
        .into_iter()
        .fold(("".to_string(), 1), |acc, x| match x {
            QueryCondition::NoOp => acc,
            _ => {
                let (q, i) = acc;
                let cond = query_cond_to_string(x, i);
                (format!("{q} and {cond}"), i + 1)
            }
        });
    where_part
}

pub fn generate_count<'a>(
    table: &'static str,
    query_conditions: &'a Vec<QueryCondition<'a>>,
) -> (String, Vec<&'a (dyn ToSql + Sync)>) {
    let base_query = format!("select count(*) from {table}");
    if query_conditions.is_empty() {
        (base_query, vec![])
    } else {
        let where_part = where_string_from_query_conds(query_conditions);
        // let (where_part, _) = query_conditions
        //     .into_iter()
        //     .fold(("".to_string(), 1), |acc, x| {
        //         let (q, i) = acc;
        //         let q_string = query_cond_to_string(x, i);
        //         (format!("{q} and {q_string}"), i + 1)
        //     });
        let query_with_where = format!("{base_query} where 1 = 1 {where_part}");
        let params = query_conditions
            .into_iter()
            .map(|x| match x {
                QueryCondition::Eq(_, p) => *p,
                QueryCondition::Neq(_, p) => *p,
                QueryCondition::Gt(_, p) => *p,
                QueryCondition::Gte(_, p) => *p,
                QueryCondition::Lt(_, p) => *p,
                QueryCondition::Lte(_, p) => *p,
                QueryCondition::In(_, p) => *p,
                QueryCondition::Nin(_, p) => *p,
                QueryCondition::Like(_, p) => *p,
                QueryCondition::NLike(_, p) => *p,
                QueryCondition::FuzzySearch(_, p) => *p,
                QueryCondition::NoOp => &true,
            })
            .collect();
        (query_with_where, params)
    }
}

pub fn generate_select<'a>(
    table: &'static str,
    query_conditions: &'a Vec<QueryCondition<'a>>,
    sort_condition_opt: &'a Option<SortCond>,
) -> (String, Vec<&'a (dyn ToSql + Sync)>) {
    let base_query = format!("select * from {table}");
    if query_conditions.is_empty() {
        (base_query, vec![])
    } else {
        let where_part = where_string_from_query_conds(query_conditions);
        // let (where_part, _) = query_conditions
        //     .into_iter()
        //     .fold(("".to_string(), 1), |acc, x| {
        //         let (q, i) = acc;
        //         let q_string = query_cond_to_string(x, i);
        //         (format!("{q} and {q_string}"), i + 1)
        //     });
        let query_with_where = format!("{base_query} where 1 = 1 {where_part}");
        let params = query_conditions
            .into_iter()
            .map(|x| match x {
                QueryCondition::Eq(_, p) => *p,
                QueryCondition::Neq(_, p) => *p,
                QueryCondition::Gt(_, p) => *p,
                QueryCondition::Gte(_, p) => *p,
                QueryCondition::Lt(_, p) => *p,
                QueryCondition::Lte(_, p) => *p,
                QueryCondition::In(_, p) => *p,
                QueryCondition::Nin(_, p) => *p,
                QueryCondition::Like(_, p) => *p,
                QueryCondition::NLike(_, p) => *p,
                QueryCondition::FuzzySearch(_, p) => *p,
                QueryCondition::NoOp => &true,
            })
            .collect();
        let query_with_sort: String = match sort_condition_opt {
            Some(sort_condition) => {
                let sort = sort_from_sort_cond(sort_condition);
                format!("{query_with_where} {sort}")
            }
            None => query_with_where,
        };
        (query_with_sort, params)
    }
}

pub fn generate_select_limit_page<'a>(
    table: &'static str,
    query_conditions: &'a Vec<QueryCondition<'a>>,
    sort_condition_opt: &'a Option<SortCond>,
    limit: u32,
    page: u32,
) -> (String, Vec<&'a (dyn ToSql + Sync)>) {
    let base_query = format!("select * from {table}");
    if query_conditions.is_empty() {
        (base_query, vec![])
    } else {
        let where_part = where_string_from_query_conds(query_conditions);
        // let (where_part, _) = query_conditions
        //     .into_iter()
        //     .fold(("".to_string(), 1), |acc, x| {
        //         let (q, i) = acc;
        //         let q_string = query_cond_to_string(x, i);
        //         (format!("{q} and {q_string}"), i + 1)
        //     });
        let offset = limit * (page - 1);
        let query_with_where = format!("{base_query} where {where_part}");
        let params = query_conditions
            .into_iter()
            .map(|x| match x {
                QueryCondition::Eq(_, p) => *p,
                QueryCondition::Neq(_, p) => *p,
                QueryCondition::Gt(_, p) => *p,
                QueryCondition::Gte(_, p) => *p,
                QueryCondition::Lt(_, p) => *p,
                QueryCondition::Lte(_, p) => *p,
                QueryCondition::In(_, p) => *p,
                QueryCondition::Nin(_, p) => *p,
                QueryCondition::Like(_, p) => *p,
                QueryCondition::NLike(_, p) => *p,
                QueryCondition::FuzzySearch(_, p) => *p,
                QueryCondition::NoOp => &true,
            })
            .collect();
        let query_with_sort: String = match sort_condition_opt {
            Some(sort_condition) => {
                let sort = sort_from_sort_cond(sort_condition);
                format!("{query_with_where} {sort} limit {limit} offset {offset}")
            }
            None => format!("{query_with_where} limit {limit} offset {offset}"),
        };
        (query_with_sort, params)
    }
}

pub fn generate_select_2<V: ToSql + Sync + Clone>(
    table: &'static str,
    query_conditions: Vec<QueryCondition2<V>>,
    sort_condition_opt: &Option<SortCond>,
) -> (String, Vec<V>) {
    let base_query = format!("select * from {table}");
    if query_conditions.is_empty() {
        (base_query, vec![])
    } else {
        let (where_part, _) = query_conditions.iter().fold(("".to_string(), 1), |acc, x| {
            let (q, i) = acc;
            let q_cond = x.clone();
            let q_string = query_cond_to_string_2(q_cond, i);
            (format!("{q} and {q_string}"), i + 1)
        });
        let query_with_where = format!("{base_query} where 1 = 1 {where_part}");
        let params = params_from_query_conds_2(query_conditions);
        let query_with_sort: String = match sort_condition_opt {
            Some(sort_condition) => {
                let sort = sort_from_sort_cond(sort_condition);
                format!("{query_with_where} {sort}")
            }
            None => query_with_where,
        };
        (query_with_sort, params)
    }
}

/*
pub fn generate_select_<'a>(
    table: &'static str,
    query_conditions: &'a Vec<QueryCond>,
    sort_condition_opt: &'a Option<SortCond>,
) -> (String, Vec<ValueTwo>) {
    let base_query = format!("select * from {table}");
    if query_conditions.is_empty() {
        (base_query, vec![])
    } else {
        let b_q_c: &Vec<QueryCond> = &query_conditions;
        let (where_part, _) = b_q_c.into_iter().fold(("".to_string(), 1), |acc, x| {
            let (q, i) = acc;
            (format!("{} and {}", q, query_cond_to_string_(&x, i)), i + 1)
        });
        let query_with_where = format!("{} where 1 = 1 {}", base_query, where_part);
        let params = query_conditions
            .into_iter()
            .map(|x| match x {
                QueryCond::Eq(_, p) => *p,
                QueryCond::Neq(_, p) => *p,
                QueryCond::Gt(_, p) => *p,
                QueryCond::Gte(_, p) => *p,
                QueryCond::Lt(_, p) => *p,
                QueryCond::Lte(_, p) => *p,
                QueryCond::In(_, p) => *p,
                QueryCond::Nin(_, p) => *p,
                QueryCond::Like(_, p) => *p,
                QueryCond::NLike(_, p) => *p,
                QueryCond::FuzzySearch(_, p) => *p,
            })
            .collect();
        let query_with_sort: String = match sort_condition_opt {
            Some(sort_condition) => format!(
                "{} {}",
                query_with_where,
                sort_from_sort_cond(&sort_condition)
            ),
            None => query_with_where,
        };
        (query_with_sort, params)
    }
}
*/

pub fn select_all<'a, F: Fn(Row) -> A + Send + 'static, A>(
    trans: &'a Transaction<'a>,
    table: &'static str,
    query_conditions: &'a Vec<QueryCondition<'a>>,
    sort_cond_opt: &'a Option<SortCond>,
    map_row: F,
) -> impl Future<Output = Result<Vec<A>, Error>> + 'a {
    async move {
        let (query, params) = generate_select(table, query_conditions, sort_cond_opt);
        let stmt = trans.prepare(&query).await?;
        let rows = trans.query(&stmt, params.as_slice()).await?;
        Ok(rows.into_iter().map(map_row).collect())
    }
}

/*
pub fn select_all_2<'a, F: Fn(Row) -> A + Send + 'static, A, V: ToSql + Sync + Clone>(
    trans: &'a Transaction<'a>,
    table: &'static str,
    query_conditions: Vec<QueryCondition2<V>>,
    sort_cond_opt: &'a Option<SortCond>,
    map_row: F,
) -> impl Future<Output = Result<Vec<A>, Error>> + 'a {
    async move {
        let (query, params) = generate_select_2(table, query_conditions, sort_cond_opt);
        let stmt = trans.prepare(&query).await?;
        let params_ = params.iter().map(|x| x).collect::<Vec<&V>>();
        let rows = trans.query(&stmt, params_.as_slice()).await?;
        Ok(rows.into_iter().map(map_row).collect())
    }
}
*/

pub fn select_all_to_map<'a, K: Eq + Hash, A, F: Fn(Row) -> (K, A) + Send + 'static>(
    trans: &'a Transaction<'a>,
    table: &'static str,
    query_conditions: &'a Vec<QueryCondition<'a>>,
    sort_cond_opt: &'a Option<SortCond>,
    map_row: F,
) -> impl Future<Output = Result<HashMap<K, A>, Error>> + 'a {
    async move {
        let (query, params) = generate_select(table, query_conditions, sort_cond_opt);
        let stmt = trans.prepare(&query).await?;
        let rows = trans.query(&stmt, params.as_slice()).await?;
        let mut hash_map = HashMap::new();
        for r in rows {
            let (k, v) = map_row(r);
            hash_map.insert(k, v);
        }
        Ok(hash_map)
    }
}

/*
pub async fn select_all_<'a, F: Fn(Row) -> A + Send + 'static, A>(
    trans: &'a Transaction<'a>,
    table: &'static str,
    query_conditions: Vec<QueryCond>,
    sort_cond_opt: Option<SortCond>,
    map_row: F,
) -> Result<Vec<A>, Error> {
    let (query, params) = generate_select(table, &query_conditions, &sort_cond_opt);
    let stmt = trans.prepare(&query).await?;
    let params_derefed = ref_from_box_vec(params.as_slice());
    let rows = trans.query(&stmt, params_derefed.as_slice()).await?;
    Ok(rows.into_iter().map(map_row).collect())
}
*/

pub fn string_from_string_ref(s: &String) -> String {
    s.clone()
}

trait ClonableSql: ToSql + Clone {}

pub fn ref_from_box_vec(x: &[Box<dyn ToSql + Sync>]) -> Vec<&(dyn ToSql + Sync)> {
    x.iter().map(|y| &**y).collect()
}

/*
pub fn select_raw<'a, F: Fn(Result<Row, tokio_postgres::Error>) -> A + Send + 'static, A>(
    client: &'a Transaction<'a>,
    table: &'static str,
    query_conditions: &'a Vec<QueryCondition<'a>>,
    sort_condition_opt: &'a Option<SortCond>,
    map_row: F,
) -> impl Future<Output = Result<impl Stream<Item = A>, RepoError>> + 'a {
    async move {
        let (query, params) = generate_select(table, query_conditions, sort_condition_opt);
        let stmt = client
            .prepare(&query)
            .await
            .map_err(|e| RepoError::RepoError(e.to_string()))?;
        let rows = client
            .query_raw(&stmt, params.into_iter())
            .await
            .map_err(|e| RepoError::RepoError(e.to_string()))?;
        Ok(rows.map(map_row))
    }
}
*/

pub fn field_names_without_id(fields: &[&str]) -> Vec<String> {
    fields
        .iter()
        .map(|x| x.to_string())
        .filter(|x| x != &"id".to_string())
        .collect()
}

pub async fn select<'a, F: Fn(Row) -> A + Send + 'static, A>(
    client: &Transaction<'a>,
    table: &'static str,
    query_conditions: &'a Vec<QueryCondition<'a>>,
    from_row: F,
) -> Result<Option<A>, Error> {
    let (query, params) = generate_select(table, query_conditions, &None);
    let stmt = client.prepare(&query).await?;
    let row_opt = client.query_opt(&stmt, params.as_slice()).await?;
    Ok(row_opt.map(from_row))
}

pub async fn query_stream<'a, T, U: ToSql>(
    client: &Client,
    query: &str,
    params: &[U],
) -> Result<RowStream, Error> {
    let stmt = client.prepare(query).await?;
    client.query_raw(&stmt, params).await.map_err(|e| e.into())
}

pub async fn query_stream_<T, U: ToSql>(
    client: &Client,
    query: &str,
    params: &[U],
    f: impl Fn(Row) -> T,
) -> Result<Vec<T>, Error> {
    let stmt = client.prepare(query).await?;
    let mut row_stream = client.query_raw(&stmt, params).await?;
    pin_mut!(row_stream);
    let mut v = vec![];
    while let Some(value) = row_stream.try_next().await? {
        v.push(f(value));
    }
    Ok(v)
}

pub async fn with_stream<T, U: ToSql>(
    client: &Client,
    query: &str,
    params: &[U],
    f: impl Fn(RowStream) -> Result<T, Error>,
) -> Result<T, Error> {
    let stmt = client.prepare(query).await?;
    let row_stream = client.query_raw(&stmt, params).await?;
    f(row_stream)
}

// todo: move to common
pub struct Page<A> {
    pub items: Vec<A>,
    pub limit: u32,
    pub page: u32,
    pub total: u32,
}

pub fn fetch_page<'a, F: Fn(Row) -> A + Send + 'static, A>(
    client: &'a Client,
    table: &'static str,
    query_conditions: &'a Vec<QueryCondition<'a>>,
    limit: u32,
    page: u32,
    sort_condition_opt: &'a Option<SortCond>,
    map_row: F,
) -> impl Future<Output = Result<Page<A>, Error>> + 'a {
    async move {
        let (query, params) = generate_select(table, query_conditions, sort_condition_opt);
        let (count_query, count_params) = generate_count(table, query_conditions);
        let stmt = client.prepare(&query).await?;
        let count_stmt = client.prepare(&count_query).await?;
        let rows = client.query(&stmt, params.as_slice()).await?;
        let count_row = client
            .query_one(&count_stmt, count_params.as_slice())
            .await?;
        let items = rows.into_iter().map(map_row).collect();
        let total = count_row.try_get(0)?;
        Ok(Page {
            items,
            limit,
            page,
            total,
        })
    }
}

pub async fn fetch_page_from_query<'a, T>(
    client: &Client,
    base_query: &'static str,
    conds: &Vec<QueryCondition<'a>>,
    sort_cond: &Option<SortCond>,
    limit: u32,
    page: u32,
    map_row: impl Fn(Row) -> T,
) -> Result<Page<T>, anyhow::Error> {
    let mut cond_strings: String = "".to_string();
    let mut params = &mut vec![];
    let mut idx: u16 = 0;
    for cond in conds {
        let cond_string = query_cond_to_string(&cond, idx);
        cond_strings = if idx == 0 {
            format!("{cond_string}")
        } else {
            format!("{cond_strings} and {cond_string}")
        };
        idx = idx + 1;

        let v = extract_value(&cond);
        params.push(v);
    }
    let sort_string = sort_cond.clone().map(|x| sort_from_sort_cond(&x)).unwrap_or("".to_string());
    let offset = limit * (page -1);
    let limit_offset_string = format!("limit {limit} offset {offset}");
    let full_query_string = format!("{base_query} where {cond_strings} {sort_string} {limit_offset_string}");
    let stmt = client.prepare(full_query_string.as_str()).await?;
    let rows = client.query(&stmt, params.as_slice()).await?;
    let items = rows.into_iter().map(map_row).collect();
    let total = 10;
    Ok(Page {
        items,
        limit,
        page,
        total,
    })
}

pub enum SqlParam {
    Int(i32),
    VarChar(String),
    Uuid(Uuid),
    Float(f32),
    Double(f64),
}

pub enum Qcond {
    Eq(Field, SqlParam),
    GtEq(Field, SqlParam),
}

pub fn q_cond_to_string(q_cond: &Qcond, idx: u8) -> String {
    match q_cond {
        Qcond::Eq(field, param) => format!("{field} = ${idx}"),
        Qcond::GtEq(field, param) => format!("{field} >= ${idx}"),
    }
}

type Val = dyn ToSql + Sync;
/*
pub fn params_to_values(param: SqlParam) -> Box<(dyn ToSql + Sync)> {
    Box::new(match param {
        SqlParam::Int(x) => x,
        SqlParam::Uuid(x) => x,
        SqlParam::VarChar(x) => x.as_str(),
        SqlParam::Float(x) => x,
        SqlParam::Double(x) => x,
    })
}
 */

trait Foo {
    fn foo(&self) -> u8 {
        0
    }
}

pub fn gen_where(params: Vec<Qcond>) -> String {
    params
        .iter()
        .fold(("".to_string(), 1), |(q_string, idx), q_cond| {
            let string = q_cond_to_string(&q_cond, idx);
            if q_string.len() == 0 {
                (format!("{string}"), idx + 1)
            } else {
                (format!("{q_string} and {string}"), idx + 1)
            }
        })
        .0
}
