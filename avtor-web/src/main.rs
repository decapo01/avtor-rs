pub mod common;
pub mod handlers;
pub mod migrations;
pub mod operations;
pub mod postgres_common;
pub mod repo;
pub mod services;

use actix_web::{self};
use clap::{command, Parser, Subcommand};
use common::env_config::EnvConfig;
use deadpool_postgres::{Manager, Pool};
use serde::Deserialize;
use tokio_postgres::NoTls;

use log::{debug, error, info, warn};

use crate::operations::{create_super_user::create_super_user, run_server::run_server};

pub type Secret = String;

#[derive(Clone)]
pub struct HandlerData {
    pub pool: Pool,
    pub secret: Secret,
    pub env: String,
}

pub fn conn_str_from_config(config: &EnvConfig) -> String {
    format!(
        "postgres://{user}:{password}@{host}:{port}/{db}",
        user = config.db_user,
        password = config.db_pass,
        host = config.db_host,
        port = config.db_port,
        db = config.db_name.to_owned().unwrap_or("postgres".to_string()),
    )
}

fn create_pool(env_config: &EnvConfig) -> Pool {
    let mut cfg = tokio_postgres::Config::new();
    cfg.host(&env_config.db_host);
    cfg.user(&env_config.db_user);
    cfg.password(&env_config.db_pass);
    cfg.dbname(env_config.db_name.as_ref().unwrap());
    cfg.port(env_config.db_port.parse::<u16>().unwrap());
    // _todo: look up no tls
    let mgr = Manager::new(cfg, NoTls);
    Pool::builder(mgr).max_size(16).build().unwrap()
}

#[derive(Subcommand, Debug)]
enum Commands {
    /// Run Migrations
    RunMigrations,
    /// Run Server
    RunServer {
        #[arg(short, long)]
        port: u16,
    },
    CreateSuperUser,
}

#[derive(Parser, Default, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    #[command(subcommand)]
    command: Option<Commands>,
}

#[derive(Clone)]
pub struct FooStruct {
    name: String,
    id: u32,
}

fn io_error_from_anyhow_error(anyhow_error: anyhow::Error) -> std::io::Error {
    std::io::Error::new(std::io::ErrorKind::Other, anyhow_error.to_string())
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::init();

    info!("parsing env vars");
    let env_config = envy::from_env::<EnvConfig>().unwrap();

    info!("Connecting to db");
    let conn_str = conn_str_from_config(&env_config);
    let pool = create_pool(&env_config);
    let (mut _client, conn) = tokio_postgres::connect(&conn_str, NoTls).await.unwrap();
    tokio::spawn(async move {
        if let Err(e) = conn.await {
            eprintln!("db connection error: {}", e);
        }
    });

    info!("parsing args");
    let args = Args::parse();
    match &args.command {
        Some(Commands::RunMigrations) => {
            info!("running migrations");
            Ok(())
        }
        Some(Commands::RunServer { port }) => {
            info!("running server on port {port}");
            run_server(pool.clone(), env_config).await?;
            Ok(())
        }
        Some(Commands::CreateSuperUser) => {
            info!("Creating super user");
            create_super_user(pool.clone(), env_config)
                .await
                .map_err(io_error_from_anyhow_error)?;
            Ok(())
        }
        None => Ok(()),
    }
}
