use std::ops::Deref;

use avtor_core::models::app_view_models::AppViewModel;
use postgres_types::{FromSql, ToSql};
use serde::{Deserialize, Serialize};
use tokio_postgres::Row;
use uuid::Uuid;

// pub const QUERY: &'static str = r"
// select
//     apps.*,
//     (select count(*) from accounts where accounts.app_id = apps.id) as account_count,
//     (select count(*) from users join accounts on account.id = users.account_id where accounts.app_id = app.id) as user_count,
//     (select count(*) from invitations join accounts on accounts.id = invitations.account_id where accounts.app_id = apps.id) as invitation_count,
// from
//     apps
// ";

pub const QUERY: &'static str = r"
select
    accounts.*,
    (select count(*) from users join accounts on account.id = users.account_id where accounts.app_id = app.id) as user_count,
    (select count(*) from invitations join accounts on accounts.id = invitations.account_id where accounts.app_id = apps.id) as invitation_count,
from
    accounts
";

pub struct InternalAppViewModel(AppViewModel);

impl Deref for InternalAppViewModel {
    type Target = AppViewModel;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl From<Row> for InternalAppViewModel {
    fn from(value: Row) -> Self {
        InternalAppViewModel(AppViewModel {
            id: value.get("id"),
            name: value.get("name"),
            sign_in_redirect: value.get("sign_in_redirect"),
            sign_up_redirect: value.get("sign_up_redirect"),
            accounts: value.get("account_count"),
            users: value.get("user_count"),
            invites: value.get("invite_count"),
        })
    }
}

pub enum AppViewModelConditions {}

#[derive(Serialize, Deserialize, Debug, ToSql, FromSql)]
pub struct AccountViewModel {
    pub id: Uuid,
    pub name: String,
    pub users: i32,
    pub invitations: i32,
}


