use deadpool_postgres::Client;
use postgres_types::{FromSql, ToSql};
use serde::{Deserialize, Serialize};
use tokio_postgres::Row;
use uuid::Uuid;

use crate::postgres_common::postgres_utils::{fetch_page, fetch_page_from_query, Page, QueryCond, QueryCondition, SortCond};


#[derive(Serialize, Deserialize, Debug, ToSql, FromSql)]
pub struct AppAccountViewModel {
    pub id: Uuid,
    pub name: String,
    pub users: i32,
    pub invitations: i32,
}

fn from_sql(row: Row) -> AppAccountViewModel {
    AppAccountViewModel {
        id: row.get("id"),
        name: row.get("name"),
        users: row.get("user_count"),
        invitations: row.get("invitations_count"),
    }
}

pub const QUERY: &'static str = r"
select
    accounts.*,
    (select count(*) from users join accounts on account.id = users.account_id where accounts.app_id = app.id) as user_count,
    (select count(*) from invitations join accounts on accounts.id = invitations.account_id where accounts.app_id = apps.id) as invitation_count,
from
    accounts";

pub struct AppAccountViewModelConditions {
    pub id_eq: Option<Uuid>,
    pub name_eq: Option<String>,
    pub name_like: Option<String>,
    pub name_fuzzy: Option<String>,
    pub count_lt: Option<i32>,
    pub sort_by: Option<String>,
    pub sort_order: Option<String>,
}

impl<'a> From<&'a AppAccountViewModelConditions> for Vec<QueryCondition<'a>> {
    fn from(value: &'a AppAccountViewModelConditions) -> Self {
        let mut conds: Vec<QueryCondition<'a>> = vec![];
        if let Some(x) = &value.id_eq {
            conds.push(QueryCondition::Eq("id", x));
        }
        if let Some(x) = &value.name_eq {
            conds.push(QueryCondition::Eq("name", x));
        }
        if let Some(x) = &value.name_like {
            conds.push(QueryCondition::Like("name", x));
        }
        if let Some(x) = &value.name_fuzzy {
            conds.push(QueryCondition::FuzzySearch("name", x));
        }
        conds
    }
}

// fn foo<'a>(uuid: Uuid) -> Vec<QueryCondition<'a>> {
//     let cond = QueryCondition::Eq("id", &uuid.clone());
//     vec![cond]
// }

/*
pub fn this_from_that<'a>(vm_conds: &'a AppAccountViewModelConditions) -> Vec<QueryCondition<'a>> {
    let id_eq = &vm_conds.id_eq;
    vec![id_eq
        .map(|x| vec![QueryCondition::Eq("id", &x.clone())])
        .unwrap_or(vec![])]
    .into_iter()
    .flatten()
    .collect()
}
*/

// impl<'a> Into<Vec<QueryCondition<'a>>> for AppAccountViewModelConditions {
//     fn into(&'a self) -> Vec<QueryCondition<'a>> {
//         let mut conds: Vec<QueryCondition<'a>> = vec![];
//         if let Some(x) = &self.id_eq {
//             conds.push(QueryCondition::Eq("id", x));
//         }
//         if let Some(x) = &self.name_eq {
//             conds.push(QueryCondition::Eq("name", x));
//         }
//         if let Some(x) = &self.name_like {
//             conds.push(QueryCondition::Like("name", x));
//         }
//         if let Some(x) = &self.name_fuzzy {
//             conds.push(QueryCondition::FuzzySearch("name", x));
//         }
//         conds
//     }
// }

pub fn query_conditions_from_app_account_view_model_conditions(
    conditions: &AppAccountViewModelConditions,
) -> Vec<QueryCondition> {
    let mut query_conds = vec![];
    let mut where_string = "".to_string();
    if let Some(x) = &conditions.id_eq {
        query_conds.push(QueryCondition::Eq("id", x))
    }
    if let Some(x) = &conditions.name_eq {
        query_conds.push(QueryCondition::Eq("name", x))
    }
    if let Some(x) = &conditions.name_fuzzy {
        query_conds.push(QueryCondition::FuzzySearch("name", x))
    }
    query_conds
}

pub fn query_cond_from_vm_conds(conds: &AppAccountViewModelConditions) -> Vec<QueryCond> {
    vec![
        conds.id_eq.map(|x| vec![QueryCond::Eq("id", Box::new(x))]).unwrap_or(vec![])
    ].into_iter().flatten().collect()
}

pub fn map_row(row: Row) -> AppAccountViewModel {
    AppAccountViewModel {
        id: row.get("id"),
        name: row.get("name"),
        users: row.get("user_count"),
        invitations: row.get("invitation_count"),
    }
}

pub fn sort_cond_from_vm_conds(vm_cond: &AppAccountViewModelConditions) -> Option<SortCond> {
    let sort_by = match &vm_cond.sort_by {
        Some(x) => match x.as_ref() {
            "id" => Some("id".to_string()),
            "name" => Some("name".to_string()),
            "users" => Some("user_count".to_string()),
            "invitations" => Some("invitation_count".to_string()),
            _ => None
        }
        _ => None
    };
    let sort_order = match &vm_cond.sort_order  {
        Some(x) => match x.as_ref() {
            "asc" => "asc",
            "desc" => "desc",
            _ => "asc"
        }
        _ => "asc"
    };
    match sort_by {
        Some(s) => match sort_order {
            "desc" => Some(SortCond::Dsc(s.to_string())),
            _ => Some(SortCond::Asc(s.to_string()))
        },
        None => None
    }
}

pub async fn find_page(
    client: Client,
    conditions: AppAccountViewModelConditions,
    limit: u32,
    page: u32,
) -> Result<Page<AppAccountViewModel>, anyhow::Error> {
    let query_conds = query_conditions_from_app_account_view_model_conditions(&conditions);
    let sort_cond = sort_cond_from_vm_conds(&conditions);
    fetch_page_from_query(&client, QUERY, &query_conds, &sort_cond, limit, page, map_row).await
}

pub async fn do_something(client: Client) -> Result<Vec<Row>, anyhow::Error> {
    let stmt = client.prepare("select * from blah where x = $1").await?;
    let rows = client.query(&stmt, &[&1]).await?;
    Ok(rows)
}
