use crate::postgres_common::postgres_utils::QueryCondition;
use crate::postgres_common::postgres_view_model::view_model;
use avtor_core::models::{accounts::Account, invitations::Invitation};
use postgres_types::ToSql;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

use super::{invitation_repo::InvitationRowCriteriaStruct, rows::AccountRowCriteriaStruct};

view_model! {
    #[derive(Debug, Serialize, Deserialize)]
    pub struct InvitationViewModel {
        pub id: Uuid,
        pub username: String,
        pub account_id: Uuid,
        pub account_name: String,
    }
}

pub struct InvitationVm {
    pub invitation: Invitation,
    pub account: Account,
}

#[derive(Serialize, Deserialize)]
pub struct InvitationCondStruct {
    pub invitation_cond: InvitationRowCriteriaStruct,
    pub account_cond: AccountRowCriteriaStruct,
}

pub fn cond_from_struct<'a>(cond_struct: &'a InvitationCondStruct) -> Vec<QueryCondition<'a>> {
    let mut vec = vec![];
    if let Some(x) = &cond_struct.invitation_cond.id_eq {
        vec.push(QueryCondition::Eq("id", x))
    }
    vec
}
