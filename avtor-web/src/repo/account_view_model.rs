use serde::{Deserialize, Serialize};
use uuid::Uuid;

use crate::postgres_common::{postgres_utils::QueryCondition, postgres_view_model::view_model};

view_model! {
    #[derive(Debug, Serialize, Deserialize)]
    pub struct AccountViewModel {
        pub id: Uuid,
        pub name: String,
        pub user_id: String,
        pub username: String,
    }
}
