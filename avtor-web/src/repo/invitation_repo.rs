use anyhow::{anyhow, Error};
use avtor_core::models::invitations::Invitation;
use futures::future::BoxFuture;
use postgres_types::{FromSql, ToSql};
use serde::{Deserialize, Serialize};
use tokio_postgres::{Row, Transaction};
use uuid::Uuid;

use crate::postgres_common::postgres_utils::*;
use crate::{postgres_common::postgres_entity::entity, repo::rows::UserRowCriteria};
use std::collections::HashSet;

use super::rows::{UserRow, UserRowRepo, USER_TABLE};

pub const INVITATIONS_TABLE: &'static str = "invitations";

entity! {
    "invitations",
    #[derive(Serialize, Deserialize, Debug, Default, FromSql, ToSql)]
    pub struct InvitationRow {
        pub id: Uuid,
        pub username: String,
    }
}

impl From<Invitation> for InvitationRow {
    fn from(value: Invitation) -> Self {
        InvitationRow {
            id: value.id,
            username: value.username,
        }
    }
}

impl From<&Invitation> for InvitationRow {
    fn from(value: &Invitation) -> Self {
        InvitationRow {
            id: value.id,
            username: value.username.clone(),
        }
    }
}

impl From<InvitationRow> for Invitation {
    fn from(value: InvitationRow) -> Self {
        Invitation {
            id: value.id,
            username: value.username,
            roles: todo!(),
            account_id: todo!(),
            expiration_date: todo!(),
            accepted_on: todo!(),
        }
    }
}

pub struct InvitationKeyValMappers;
impl InvitationKeyValMappers {
    pub fn map_key_val(row: Row) -> (Uuid, InvitationRow) {
        let invitation_row = InvitationRow::from_row(row);
        (invitation_row.id, invitation_row)
    }

    pub fn map_user_key_val(row: Row) -> (String, InvitationRow) {
        let invitation_row = InvitationRow::from_row(row);
        (invitation_row.username.clone(), invitation_row)
    }
}

#[derive(Clone)]
pub enum InvitationSaveOp {
    Insert {
        username_eq: String,
    },
    Update {
        id_not_eq: Uuid,
        username_eq: String,
    },
}

pub struct InvitationConds;
impl InvitationConds {
    pub fn unique_conds(op: InvitationSaveOp) -> Vec<InvitationRowCriteria> {
        match op {
            InvitationSaveOp::Insert { username_eq } => {
                vec![InvitationRowCriteria::UsernameEq(username_eq)]
            }
            InvitationSaveOp::Update {
                id_not_eq,
                username_eq,
            } => vec![
                InvitationRowCriteria::IdNeq(id_not_eq),
                InvitationRowCriteria::UsernameEq(username_eq),
            ],
        }
    }

    pub fn insert_conds(username: String) -> Vec<InvitationRowCriteria> {
        vec![InvitationRowCriteria::UsernameEq(username)]
    }

    pub fn update_conds(id: Uuid, username: String) -> Vec<InvitationRowCriteria> {
        vec![
            InvitationRowCriteria::IdEq(id),
            InvitationRowCriteria::UsernameEq(username),
        ]
    }

    pub fn delete_conds(id: Uuid) -> Vec<UserRowCriteria> {
        vec![UserRowCriteria::IdEq(id)]
    }
}

pub struct InvitationQueries;
impl InvitationQueries {
    pub fn insert<'a>(
        trans: &'a Transaction<'a>,
    ) -> impl Fn(&'a Invitation) -> BoxFuture<'a, Option<Error>> {
        move |invitation| {
            Box::pin(async move {
                InvitationRowRepo::insert(trans)(&invitation.into())
                    .await
                    .err()
            })
        }
    }

    pub async fn update<'a>(
        trans: &'a Transaction<'a>,
    ) -> impl FnOnce(Invitation) -> BoxFuture<'a, Option<Error>> {
        move |invitation| {
            Box::pin(async move {
                InvitationRowRepo::update(trans)(&invitation.into())
                    .await
                    .err()
            })
        }
    }

    pub async fn delete<'a>(
        trans: &'a Transaction<'a>,
    ) -> impl FnOnce(Invitation) -> BoxFuture<'a, Option<Error>> {
        move |invitation| {
            Box::pin(async move {
                InvitationRowRepo::delete(trans)(&invitation.into())
                    .await
                    .err()
            })
        }
    }

    pub fn find_unique<'a>(
        trans: &'a Transaction<'a>,
        invitation_save_op: &'a InvitationSaveOp,
    ) -> impl Fn(&'a Invitation) -> BoxFuture<'a, Option<Error>> {
        move |_invitations| {
            Box::pin(async move {
                let conds = InvitationConds::unique_conds(invitation_save_op.clone());
                InvitationRowRepo::find_by_criteria(trans)(conds)
                    .await
                    .ok()
                    .map(|_| anyhow!("Invitation Found"))
            })
        }
    }

    pub async fn find_delete_associations<'a>(
        trans: &'a Transaction<'a>,
    ) -> impl FnOnce(InvitationRow) -> BoxFuture<'a, Result<Option<UserRow>, anyhow::Error>> {
        move |invitation_row| {
            Box::pin(async move {
                let conds = InvitationConds::delete_conds(invitation_row.id);
                UserRowRepo::find_by_criteria(trans)(conds)
                    .await
                    .map_err(anyhow::Error::from)
            })
        }
    }

    pub fn check_associations<'a>() -> impl FnOnce(&'a Invitation) -> BoxFuture<'a, Option<Error>> {
        move |_| Box::pin(async { None })
    }
}
