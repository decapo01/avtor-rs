use anyhow::anyhow;
use avtor_core::models::{
    accounts::{Account, AccountId},
    users::User,
};
use futures::future::BoxFuture;
use tokio_postgres::Transaction;

use super::rows::{AccountRow, AccountRowCriteria, AccountRowRepo, UserRowCriteria, UserRowRepo};

pub enum AccountFindUniqueOp {
    Create,
    Edit,
}

pub fn row_from_entity(account: Account) -> AccountRow {
    AccountRow {
        id: account.id.0,
        name: account.name,
    }
}

pub fn entity_from_row(account_row: AccountRow) -> Account {
    Account {
        id: AccountId(account_row.id),
        name: account_row.name,
    }
}

pub struct AccountRowRepoFuncs {}

impl AccountRowRepoFuncs {
    pub fn find_unique<'a>(
        trans: &'a Transaction<'a>,
        operation: AccountFindUniqueOp,
    ) -> impl FnOnce(Account) -> BoxFuture<'a, Option<anyhow::Error>> {
        move |account| {
            Box::pin(async move {
                let account_row = row_from_entity(account);
                let conds = match operation {
                    AccountFindUniqueOp::Create => {
                        vec![AccountRowCriteria::NameEq(account_row.name)]
                    }
                    AccountFindUniqueOp::Edit => vec![
                        AccountRowCriteria::NameEq(account_row.name),
                        AccountRowCriteria::IdNLike(account_row.id),
                    ],
                };
                let res = AccountRowRepo::find_by_criteria(trans)(conds).await;
                match res {
                    Ok(Some(_)) => Some(anyhow!("user exists")),
                    Ok(None) => None,
                    Err(e) => Some(e.into()),
                }
            })
        }
    }

    pub fn check_associations<'a>(
        trans: &'a Transaction<'a>,
    ) -> impl FnOnce(Account) -> BoxFuture<'a, Option<anyhow::Error>> {
        move |account| {
            Box::pin(async move {
                let conds = vec![UserRowCriteria::AccountIdEq(account.id.0)];
                let user_opt_res = UserRowRepo::find_by_criteria(trans)(conds).await;
                match user_opt_res {
                    // todo: make a real error
                    Ok(Some(_u)) => Some(anyhow!("Users still there")),
                    Ok(None) => None,
                    Err(e) => Some(e.into()),
                }
            })
        }
    }

    pub fn insert<'a>(
        trans: &'a Transaction<'a>,
    ) -> impl FnOnce(Account) -> BoxFuture<'a, Option<anyhow::Error>> {
        move |account| {
            Box::pin(async move {
                AccountRowRepo::insert(trans)(&account.into())
                    .await
                    .map_err(|e| e.into())
                    .err()
            })
        }
    }

    pub fn update<'a>(
        trans: &'a Transaction<'a>,
    ) -> impl FnOnce(Account) -> BoxFuture<'a, Option<anyhow::Error>> {
        move |account| {
            Box::pin(async move {
                AccountRowRepo::update(trans)(&account.into())
                    .await
                    .map_err(|e| e.into())
                    .err()
            })
        }
    }
}
