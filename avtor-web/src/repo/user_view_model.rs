use std::{collections::HashMap, pin::Pin, vec};

use postgres_types::ToSql;
use serde::{Deserialize, Serialize};
use tokio_postgres::Row;
use uuid::Uuid;

use crate::postgres_common::{
    postgres_utils::{QueryCondition, Value},
    postgres_view_model::view_model,
};

view_model! {
    #[derive(Debug, Serialize, Deserialize)]
    pub struct UserViewModel {
        pub id: Uuid,
        pub username: String,
        pub account_id: Uuid,
        pub account_name: String,
    }
}

pub const id: &'static str = "id";
pub const username: &'static str = "username";
pub const account_id: &'static str = "accounts.id";
pub const account_name: &'static str = "accounts.name";

pub fn user_vm_map() -> HashMap<&'static str, &'static str> {
    HashMap::from([
        ("id", "u.id"),
        ("username", "u.username"),
        ("account_id", "a.id"),
        ("account_name", "a.id"),
    ])
}

pub const USER_VIEW_MODEL_QUERY: &'static str = r"
    select
        u.id,
        u.username,
        a.id,
        a.name
    from
        users u
    join
        accounts a on u.account_id = a.id";

impl From<Row> for UserViewModel {
    fn from(value: Row) -> Self {
        UserViewModel {
            id: value.get("u.id"),
            username: value.get("u.username"),
            account_id: value.get("a.id"),
            account_name: value.get("a.name"),
        }
    }
}

pub struct UserViewModelCritStruct {
    pub id_eq: Option<Uuid>,
    pub username_eq: Option<String>,
    pub account_id_eq: Option<Uuid>,
    pub account_name_eq: Option<String>,
    pub id_neq: Option<Uuid>,
    pub username_neq: Option<String>,
    pub account_id_neq: Option<Uuid>,
    pub account_name_neq: Option<String>,

    pub id_in: Option<Uuid>,
    pub username_in: Option<Vec<String>>,
    pub account_id_in: Option<Uuid>,
    pub account_name_in: Option<String>,

    pub id_nin: Option<Uuid>,
    pub username_nin: Option<Vec<String>>,
    pub account_id_nin: Option<Uuid>,
    pub account_name_nin: Option<String>,

    pub id_like: Option<Uuid>,
    pub username_like: Option<String>,
    pub account_id_like: Option<Uuid>,
    pub account_name_like: Option<String>,

    pub id_nlike: Option<Uuid>,
    pub username_nlike: Option<String>,
    pub account_id_nlike: Option<Uuid>,
    pub account_name_nlike: Option<String>,

    pub id_fuzzy_search: Option<Uuid>,
    pub username_fuzzy_search: Option<String>,
    pub account_id_fuzzy_search: Option<Uuid>,
    pub account_name_fuzzy_search: Option<String>,
}

pub fn foo<'a, F>(x: Option<&'a (dyn ToSql + Sync)>, f: F) -> QueryCondition<'a>
where
    F: FnOnce(&'a (dyn ToSql + Sync)) -> QueryCondition<'a>,
{
    match x {
        Some(y) => f(y),
        None => QueryCondition::NoOp,
    }
}

pub fn bar<'a>(
    x: &'a (dyn ToSql + Sync),
    f: impl FnOnce(&'a (dyn ToSql + Sync)) -> QueryCondition<'a>,
) -> QueryCondition<'a> {
    f(x)
}

type X = (dyn ToSql + Sync);

struct MyX<'a>(&'a X);

// fn baz<'a>(
//     x: &'a (dyn ToSql + Sync),
//     f: impl FnOnce(&'a (dyn ToSql + Sync)) -> Box<'a + (dyn ToSql + Sync)>,
// ) -> MyX<'a> {
//     let z = f(x);
//     MyX(z)
// }

fn shout(x: &'static str) {
    print!("{}", x)
}

struct SqlBox<'a>(&'a (dyn ToSql + Sync));

fn poo<'a>(name: &'a Option<String>) -> Vec<&'a (dyn ToSql + Sync)> {
    let mut vec = vec![];
    if let Some(s) = &name {
        // let b = SqlBox(s);
        let z: &(dyn ToSql + Sync) = s;
        vec.push(z);
    }
    vec
}

pub fn query_conds_from_crit_struct<'a>(
    user_view_model_crit_struct: &'a UserViewModelCritStruct,
) -> Vec<QueryCondition<'a>> {
    let mut vec = vec![];
    if let Some(x) = &user_view_model_crit_struct.id_eq {
        vec.push(QueryCondition::Eq(id, x));
    }
    if let Some(x) = &user_view_model_crit_struct.id_neq {
        vec.push(QueryCondition::Neq(id, x));
    }
    if let Some(x) = &user_view_model_crit_struct.id_in {
        vec.push(QueryCondition::In(id, x));
    }
    if let Some(x) = &user_view_model_crit_struct.id_nin {
        vec.push(QueryCondition::Nin(id, x));
    }
    if let Some(x) = &user_view_model_crit_struct.id_fuzzy_search {
        vec.push(QueryCondition::FuzzySearch(id, x));
    }
    if let Some(x) = &user_view_model_crit_struct.username_eq {
        vec.push(QueryCondition::Eq(id, x));
    }
    if let Some(x) = &user_view_model_crit_struct.username_neq {
        vec.push(QueryCondition::Neq(id, x));
    }
    if let Some(x) = &user_view_model_crit_struct.username_in {
        vec.push(QueryCondition::In(username, x));
    }
    if let Some(x) = &user_view_model_crit_struct.username_nin {
        vec.push(QueryCondition::In(username, x));
    }
    if let Some(x) = &user_view_model_crit_struct.account_id_eq {
        vec.push(QueryCondition::Eq(account_id, x));
    }
    if let Some(x) = &user_view_model_crit_struct.account_id_neq {
        vec.push(QueryCondition::Neq(account_id, x));
    }
    if let Some(x) = &user_view_model_crit_struct.account_name_eq {
        vec.push(QueryCondition::Eq(account_name, x));
    }
    vec
}

/*
fn foo_from_bar(cond_struct: UserViewModelCondStruct) -> Vec<AccountCond> {
    let mut conds = vec![];
    if let Some(x) = cond_struct.id_eq {
        conds.push(AccountCond::IdEq(x))
    }
    conds
}

fn bar_from_foo<'a>(account_cond: &'a AccountCond) -> QueryCondition<'a> {
    match account_cond {
        AccountCond::IdEq(id) => QueryCondition::Eq("id".to_string(), id),
        AccountCond::IdNeq(id) => QueryCondition::Neq("id".to_string(), id),
    }
}

fn blah_from_blah<'a>(account_conds: Vec<&'a AccountCond>) -> Vec<QueryCondition<'a>> {
    account_conds.iter().map(|c| bar_from_foo(c)).collect()
}

fn blah_to_blah<'a>(cond_struct: AccCond<'a>, alt: &'a Uuid) -> Vec<QueryCondition<'a>> {
    // let mut cond_vec = vec![];
    // if let Some(x) = cond_struct.id_eq {
    //     let id = x.clone();
    //     let q_cond = QueryCondition::Eq("id".to_string(), id);
    //     cond_vec.push(q_cond);
    // }
    let y = match cond_struct.id_eq {
        Some(x) => QueryCondition::Eq("id".to_string(), x),
        None => QueryCondition::Eq("id".to_string(), alt),
    };
    vec![y]
}

*/
