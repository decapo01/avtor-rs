use avtor_core::models::{
    accounts::{Account, AccountId},
    users::{CreateSuperUserError, Roles, User, UserId},
};
use chrono::{DateTime, Utc};
use deadpool_postgres::{Client, Pool};
use postgres_types::{FromSql, ToSql};
use serde::{Deserialize, Serialize};
use uuid::Uuid;
use validator::Validate;

use crate::common::with_trans;
use crate::postgres_common::postgres_entity::entity;

use crate::postgres_common::postgres_utils::*;
use anyhow::anyhow;
use futures::future::BoxFuture;
use std::{collections::HashSet, fmt::Display, process::id};
use tokio_postgres::{Row, Transaction};

/*
impl From<anyhow::Error> for CreateSuperUserError {
    fn from(_: anyhow::Error) -> Self {
        CreateSuperUserError::UnknownError
    }
}
*/

pub const USER_TABLE: &'static str = "users";
pub const INTERNAL_USERS_TABLE: &'static str = "internal_users";
pub const ACCOUNT_TABLE: &'static str = "accounts";
pub const UNREGISTERED_TABLE: &'static str = "unregistered_users";
pub const CONFIG_TABLE: &'static str = "config";
pub const INVITATION_TABLE: &'static str = "invitations";
pub const LOGIN_ATTEMPT_TABLE: &'static str = "login_attempts";
pub const BLOCKED_IP_TABLE: &'static str = "blocked_ip";

entity! {
    CONFIG_TABLE,
    #[derive(Serialize, Deserialize, Debug, Default)]
    pub struct ConfigRow {
        id: Uuid,
        allow_auto_registration: bool,
        allow_external_invitations: bool,
        allow_login_attempts: i32,
    }
}

entity! {
    ACCOUNT_TABLE,
    #[derive(Serialize, Deserialize, Debug, Default)]
    pub struct AccountRow {
        id: Uuid,
        name: String,
    }
}

impl From<AccountRow> for Account {
    fn from(value: AccountRow) -> Self {
        Account {
            id: AccountId(value.id),
            name: value.name,
        }
    }
}

impl From<Account> for AccountRow {
    fn from(value: Account) -> Self {
        AccountRow {
            id: value.id.0,
            name: value.name,
        }
    }
}

entity! {
    USER_TABLE,
    #[derive(Serialize, Deserialize, Debug, Default)]
    pub struct UserRow {
        id: Uuid,
        username: String,
        password: String,
        roles: String,
        account_id: Uuid,
        locked: bool,
        active: bool,
    }
}

impl From<UserRow> for User {
    fn from(value: UserRow) -> Self {
        User {
            id: UserId(value.id),
            username: value.username,
            password: value.password,
            roles: value.roles,
            account_id: AccountId(value.account_id),
            locked: value.locked,
            active: value.active,
        }
    }
}

entity! {
    UNREGISTERED_TABLE,
    #[derive(Serialize, Deserialize, Debug, Default)]
    pub struct UnregisteredUserRow {
        id: Uuid,
        username: String,
        roles: String,
        account_id: Uuid,
        expirations: i16,
    }
}

entity! {
    INTERNAL_USERS_TABLE,
    #[derive(Serialize, Deserialize, Debug, Default)]
    pub struct InternalUserRow {
        pub id: Uuid,
        pub email: String,
        pub password: String,
        pub roles: String,
    }
}

entity! {
    INVITATION_TABLE,
    pub struct InvitationRow {
        pub id: Uuid,
        pub username: String,
        pub roles: String,
        pub account_id: String,
        pub expiration_date: DateTime<Utc>,
        pub accepted_on: Option<DateTime<Utc>>,
    }
}

entity! {
    LOGIN_ATTEMPT_TABLE,
    #[derive(Serialize, Deserialize, Debug, Default)]
    pub struct LoginAttemptRow {
        pub id: Uuid,
        pub ip: String,
        pub attempted_on: DateTime<Utc>,
    }
}

entity! {
    BLOCKED_IP_TABLE,
    #[derive(Serialize, Deserialize, Debug, Default)]
    pub struct BlockedIpRow {
        pub id: Uuid,
        pub ip: String,
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct AccountViewModel {
    pub id: Uuid,
    pub name: String,
    pub user_id: Uuid,
    pub username: String,
}

pub enum Field {
    Id,
    Name,
    UserId,
    Username,
}

impl From<Field> for String {
    fn from(value: Field) -> Self {
        match value {
            Field::Id => "id".to_string(),
            Field::Name => "name".to_string(),
            Field::UserId => "u.user_id".to_string(),
            Field::Username => "u.username".to_string(),
        }
    }
}

impl ToString for Field {
    fn to_string(&self) -> String {
        match self {
            Field::Id => "id".to_string(),
            Field::Name => "name".to_string(),
            Field::UserId => "user_id".to_string(),
            Field::Username => "username".to_string(),
        }
    }
}

enum CondOp<T: ToString> {
    Eq(T),
    NEq(T),
}

fn to_cond_string<T: ToString>(op: &CondOp<T>, idx: u32) -> String {
    match op {
        CondOp::Eq(f) => format!("{} = ${idx}", f.to_string()),
        CondOp::NEq(f) => format!("{} != ${idx}", f.to_string()),
    }
}

fn to_sql_string<T: ToString>(ops: Vec<CondOp<T>>) -> String {
    let (q_str, _) = ops.iter().fold(("".to_string(), 1), |(q_str, idx), op| {
        if q_str == "".to_string() {
            (to_cond_string(op, idx), idx + 1)
        } else {
            let cond_str = to_cond_string(op, idx);
            let new_q_str = format!("{q_str} and {cond_str}");
            (new_q_str, idx + 1)
        }
    });
    q_str
}

impl From<Row> for AccountViewModel {
    fn from(value: Row) -> Self {
        AccountViewModel {
            id: value.get("id"),
            name: value.get("name"),
            user_id: value.get("users.id"),
            username: value.get("users.username"),
        }
    }
}

struct AccountConditions;
impl AccountConditions {
    fn insert_conditions(name: String) -> Vec<AccountRowCriteria> {
        vec![AccountRowCriteria::NameEq(name)]
    }
    fn update_conditions(id: Uuid, name: String) -> Vec<AccountRowCriteria> {
        vec![
            AccountRowCriteria::NameEq(name),
            AccountRowCriteria::IdNeq(id),
        ]
    }
    fn delete_conditions(id: Uuid) -> (Vec<UserRowCriteria>, Vec<UnregisteredUserRowCriteria>) {
        (
            vec![UserRowCriteria::AccountIdEq(id)],
            vec![UnregisteredUserRowCriteria::AccountIdEq(id)],
        )
    }
}

impl From<RepoError> for CreateSuperUserError {
    fn from(value: RepoError) -> Self {
        CreateSuperUserError::RepoError(value.to_string())
    }
}

struct UserMutateConditions;
impl UserMutateConditions {
    fn insert_conditions(id: String) -> Vec<UserRowCriteria> {
        vec![UserRowCriteria::UsernameEq(id)]
    }
    fn update_conditions(id: Uuid, name: String) -> Vec<UserRowCriteria> {
        vec![
            UserRowCriteria::UsernameNeq(name),
            UserRowCriteria::IdNeq(id),
        ]
    }
    fn delete_conditions(id: Uuid) -> Vec<UnregisteredUserRowCriteria> {
        vec![UnregisteredUserRowCriteria::IdEq(id)]
    }
}

pub struct SuperUserQueries;
impl SuperUserQueries {
    pub fn find_super_user<'a>(
        trans: &'a Transaction,
    ) -> impl Fn() -> BoxFuture<'a, Result<Option<User>, CreateSuperUserError>> {
        move || {
            Box::pin(async move {
                let super_user = Roles::SuperUser.to_snake_string();
                let role = format!("%{super_user}%");
                let user_conds = vec![UserRowCriteria::RolesLike(role)];
                UserRowRepo::find_by_criteria(trans)(user_conds)
                    .await
                    .map(|x| x.map(|row| User::from(row)))
                    .map_err(|e| CreateSuperUserError::RepoError(e.to_string()))
            })
        }
    }

    pub fn insert<'a>(
        trans: &'a Transaction,
    ) -> impl FnOnce(User) -> BoxFuture<'a, Result<(), CreateSuperUserError>> {
        move |user| {
            Box::pin(async move {
                // let row = user_row_from_user(user);
                // UserRowRepo::insert(trans)(row).await.map_err(|x| x.into())
                Ok(())
            })
        }
    }

    pub fn update<'a>(
        pool: Pool,
    ) -> impl FnOnce(UserRow) -> BoxFuture<'a, Result<(), CreateSuperUserError>> {
        move |user: UserRow| {
            Box::pin(async move {
                with_trans(pool.clone(), |trans| {
                    Box::pin(async move {
                        UserRowRepo::update(trans)(&user)
                            .await
                            .map_err(|e| e.into())
                    })
                })
                .await
            })
        }
    }
}

/*
fn user_row_from_user(user: User) -> UserRow {
    UserRow { id: user.id.0, username: user.username, password: user.password, roles: user.roles, account_id: user.account_id, locked: , active: () }
}
*/
