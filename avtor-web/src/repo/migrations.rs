use crate::postgres_common::{postgres_entity::entity, postgres_utils::*};
use anyhow::anyhow;
use chrono::NaiveDateTime;
use futures::future::BoxFuture;
use serde::{Deserialize, Serialize};
use std::collections::HashSet;
use tokio_postgres::{Row, Transaction};
use uuid::Uuid;

pub const MIGRATION_TABLE: &'static str = "migrations";

entity! {
    MIGRATION_TABLE,
    pub struct Migration {
        pub id : Uuid,
        pub name: String,
        pub seq_order: i32,
        pub up: String,
        pub down: String,
        pub applied_on: NaiveDateTime,
    }
}

impl Default for Migration {
    fn default() -> Self {
        Self {
            id: Default::default(),
            name: Default::default(),
            seq_order: Default::default(),
            up: Default::default(),
            down: Default::default(),
            applied_on: Default::default(),
        }
    }
}

impl From<Result<Row, tokio_postgres::Error>> for Migration {
    fn from(value: Result<Row, tokio_postgres::Error>) -> Self {
        match value {
            Ok(r) => Migration::from_row(r),
            Err(_) => Migration::default(),
        }
    }
}
