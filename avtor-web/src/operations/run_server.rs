use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use avtor_core::models::config::Config;
use deadpool_postgres::Pool;
use serde::{Deserialize, Serialize};

use crate::{
    common::env_config::EnvConfig,
    common::handler_data::HandlerData,
    handlers::account_handlers::{post_create, post_update},
};

#[derive(Serialize, Deserialize)]
pub struct HomeMsg {
    msg: String,
}

async fn index() -> HttpResponse {
    HttpResponse::Ok().json(HomeMsg {
        msg: "Hello World".to_string(),
    })
}

pub async fn run_server(pool: Pool, config: EnvConfig) -> std::io::Result<()> {
    let handler_data = HandlerData {
        pool: pool,
        // todo: update to config
        secret: "1234".to_string(),
        // todo: update to config
        env: "local".to_string(),
    };
    let EnvConfig { port: port, .. } = config;
    HttpServer::new(move || {
        App::new()
            .app_data(web::Data::new(handler_data.clone()))
            .service(actix_files::Files::new("/static", "../static").show_files_listing())
            .route("/", web::get().to(index))
            .service(
                web::scope("/api").service(
                    web::scope("accounts")
                        .route("", web::post().to(post_create))
                        .route("{id}", web::post().to(post_update))
                        .route("{id}", web::delete().to(post_update)),
                ),
            )
    })
    .bind(("0.0.0.0", port))?
    .run()
    .await
}
