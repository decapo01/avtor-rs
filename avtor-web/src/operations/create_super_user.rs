use anyhow::anyhow;
use avtor_core::models::{
    accounts::{Account, AccountDto, AccountId},
    config::{self, Config},
    users::{CreateAccountError, CreateSuperUserError, UserDto},
};
use deadpool_postgres::Pool;
use futures::future::BoxFuture;
use tokio_postgres::Transaction;

use crate::{
    common::{env_config::EnvConfig, hash_password::hash_password, with_trans},
    repo::{
        account_row_repo,
        rows::{AccountRow, AccountRowCriteria, AccountRowRepo, SuperUserQueries, UserRow},
    },
    services::account_service::AccountService,
};

pub struct AccountRepo {}

impl AccountRepo {
    pub fn account_from_row(row: AccountRow) -> Account {
        Account {
            id: AccountId(row.id),
            name: row.name,
        }
    }

    pub fn find_by_id<'a>(
        trans: &'a Transaction<'a>,
    ) -> impl FnOnce(AccountId) -> BoxFuture<'a, Result<Option<Account>, anyhow::Error>> {
        move |account_id| {
            Box::pin(async move {
                let conds = vec![AccountRowCriteria::IdEq(account_id.0)];
                AccountRowRepo::find_by_criteria(trans)(conds)
                    .await
                    .map_err(|e| e.into())
                    .map(|account_row_opt| account_row_opt.map(AccountRepo::account_from_row))
            })
        }
    }

    pub fn insert<'a>(
        trans: &'a Transaction<'a>,
    ) -> impl FnOnce(Account) -> BoxFuture<'a, Result<(), anyhow::Error>> {
        move |account| {
            Box::pin(async move {
                AccountRowRepo::insert(trans)(&account.into())
                    .await
                    .map_err(|e| e.into())
            })
        }
    }
}

pub fn insert_account_for_super_user<'a>(
    trans: &'a Transaction<'a>,
) -> impl FnOnce(Account) -> BoxFuture<'a, Result<(), CreateAccountError>> {
    move |account| {
        Box::pin(async move {
            AccountRepo::insert(trans)(account)
                .await
                .map_err(|e| CreateAccountError::RepoError(e.to_string()))
        })
    }
}

pub fn find_account_by_id<'a>(
    trans: &'a Transaction<'a>,
) -> impl FnOnce(AccountId) -> BoxFuture<'a, Result<Option<Account>, CreateAccountError>> {
    move |account_id| {
        Box::pin(async move {
            AccountRepo::find_by_id(trans)(account_id)
                .await
                .map_err(|e| CreateAccountError::RepoError(e.to_string()))
        })
    }
}

pub async fn create_super_user(pool: Pool, config: EnvConfig) -> Result<(), anyhow::Error> {
    with_trans(pool, |trans| {
        Box::pin(async move {
            let hashed_pw = hash_password(&config.super_user_password)?;
            let user_dto = user_dto_from_config(config.clone(), hashed_pw);
            let account_dto = account_dto_from_config(config.clone());
            AccountService::create_super_user(
                SuperUserQueries::find_super_user(&trans),
                SuperUserQueries::insert(&trans),
                insert_account_for_super_user(&trans),
                find_account_by_id(&trans),
                &user_dto,
                &account_dto,
            )
            .await
        })
    })
    .await
}

fn user_dto_from_config(config: EnvConfig, hashed_pw: String) -> UserDto {
    UserDto {
        id: config.super_user_id,
        username: config.super_user_username,
        password: hashed_pw,
        roles: "user_user".to_string(),
        account_id: config.main_account_id,
    }
}

fn account_dto_from_config(config: EnvConfig) -> AccountDto {
    AccountDto {
        id: config.main_account_id,
        name: config.main_account_name.clone(),
    }
}
