use deadpool_postgres::Pool;

use crate::migrations::migration_01;

pub async fn run_migrations_up(pool: Pool) -> Result<(), anyhow::Error> {
    migration_01::run_migrations(pool).await
}
