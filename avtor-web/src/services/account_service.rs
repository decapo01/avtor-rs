use std::collections::HashMap;

use avtor_core::models::{
    accounts::{Account, AccountDto, AccountId},
    dtos::LoginDto,
    users::{CreateAccountError, CreateSuperUserError, User, UserDto},
    LoggedInUser, LoginError, LoginRepoError,
};
use futures::Future;
use serde::{Deserialize, Serialize};
use uuid::Uuid;
use validator::{Validate, ValidationErrors};

use crate::{
    common::service::GenSvc,
    repo::rows::{AccountRow, AccountRowRepo},
};

/*
#[derive(Serialize, Deserialize, Clone)]
pub struct LoggedInUser {
    pub id: Uuid,
    pub username: String,
    pub roles: Vec<String>,
    pub account_id: Uuid,
}
*/

// // pub struct LoginRepoError(pub String);

// // pub enum LoginError {
// //     UserNotFound,
// //     PasswordDoesNotMatch,
// //     RepoError(LoginRepoError),
// }

// todo: move somewhere
fn hash_map_from_validation_errors(e: ValidationErrors) -> HashMap<String, String> {
    let field_errors = e.field_errors();
    field_errors
        .into_iter()
        .map(|fe| {
            let (field, msgs) = fe;
            let msg = msgs.into_iter().fold("".to_string(), |acc, x| {
                if acc.is_empty() {
                    x.to_string()
                } else {
                    format!("{}, {}", acc, x.to_string())
                }
            });
            (field.to_string(), msg)
        })
        .collect()
}

fn logged_in_user_from_user(user: &User) -> LoggedInUser {
    LoggedInUser {
        id: user.id.0,
        username: user.username.clone(),
        roles: user
            .roles
            .split(",")
            .map(|x| x.to_string())
            .collect::<Vec<String>>(),
        account_id: user.account_id.0,
    }
}

type Secret = String;
type AuthToken = String;

pub struct AccountService {}

impl AccountService {
    pub async fn find_unique(account: Account) -> Result<(), String> {
        let row = AccountRow {
            id: account.id.0,
            name: account.name,
        };
        Ok(())
    }

    /*  going to use the gen server
    pub async fn create<FA, FB, FC>(
        find_unique: impl FnOnce(Account) -> FA,
        insert: impl FnOnce(Account) -> FB,
        check_associations: impl FnOnce(Account) -> FC,
        account_dto: &AccountDto,
    ) -> Result<(), String>
    where
        FA: Future<Output = Result<Option<Account>, String>>,
        FB: Future<Output = Result<(), String>>,
        FC: Future<Output = Result<(), String>>,
    {
        let validate = |account_dto: &AccountDto| -> Option<anyhow::Error> {
            match account_dto.validate() {
                Ok(_) => None,
                Err(e) => Some(e.into()),
            }
        };
        let map_dto = |dto: &AccountDto| &Account {
            id: AccountId(dto.id),
            name: dto.name,
        };

        // todo: create a real func for this
        let save_res = GenSvc::save(validate, map_dto, find_user, insert, account_dto).await;
        match save_res {
            Some(e) => Err(e),
            None => Ok(()),
        }
    }
    */

    pub async fn create_super_user<FA, FB, FC, FD>(
        find_super_user: impl Fn() -> FA,
        insert: impl FnOnce(User) -> FB,
        insert_account: impl FnOnce(Account) -> FC,
        find_account_by_id: impl FnOnce(AccountId) -> FD,
        user_dto: &UserDto,
        account_dto: &AccountDto,
    ) -> Result<(), anyhow::Error>
    where
        FA: Future<Output = Result<Option<User>, CreateSuperUserError>>,
        FB: Future<Output = Result<(), CreateSuperUserError>>,
        FC: Future<Output = Result<(), CreateAccountError>>,
        FD: Future<Output = Result<Option<Account>, CreateAccountError>>,
    {
        let _ = user_dto.validate().map_err(|e| {
            let hash_map = hash_map_from_validation_errors(e);
            CreateSuperUserError::UserInvalid(hash_map)
        })?;
        let _ = account_dto.validate().map_err(|e| {
            let hash_map = hash_map_from_validation_errors(e);
            CreateSuperUserError::AccountInvalid(hash_map)
        })?;
        let user = User::from(user_dto.clone());
        let maybe_existing_user = find_super_user().await?;
        match maybe_existing_user {
            Some(_) => Err(CreateSuperUserError::SuperUserExists.into()),
            None => {
                let maybe_existing_account = find_account_by_id(AccountId(account_dto.id))
                    .await
                    .map_err(|e| CreateSuperUserError::RepoError(e.to_string()))?;
                match maybe_existing_account {
                    Some(_) => Err(CreateSuperUserError::AccountExists.into()),
                    None => {
                        let account = Account {
                            id: AccountId(account_dto.id),
                            name: account_dto.clone().name,
                        };
                        let _ = insert_account(account)
                            .await
                            .map_err(|e| CreateSuperUserError::RepoError(e.to_string()))?;
                        let ins_res = insert(user).await;
                        match ins_res {
                            Ok(_) => Ok(()),
                            Err(e) => Err(e.into()),
                        }
                    }
                }
            }
        }
    }

    pub async fn login<FA: Send + Future<Output = Result<Option<User>, LoginRepoError>>>(
        find_user_by_username: impl FnOnce(String) -> FA,
        verify_password: impl FnOnce(&String, &String) -> Result<bool, LoginError>,
        create_auth_token: impl FnOnce(Secret, LoggedInUser) -> Result<AuthToken, LoginError>,
        login_dto: LoginDto,
        secret: Secret,
    ) -> Result<AuthToken, LoginError> {
        let user_from_repo = find_user_by_username(login_dto.username)
            .await
            .map_err(|e| LoginError::RepoError(e))?;
        match user_from_repo {
            None => Err(LoginError::UserNotFound),
            Some(u) => {
                if verify_password(&u.password, &login_dto.password)? {
                    let logged_in_user = logged_in_user_from_user(&u);
                    let auth_token = create_auth_token(secret, logged_in_user)?;
                    Ok(auth_token)
                } else {
                    Err(LoginError::PasswordDoesNotMatch)
                }
            }
        }
    }
}
