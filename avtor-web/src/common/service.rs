use anyhow::{anyhow, Error};
use futures::Future;
use std::collections::HashMap;

pub struct GenSvc;

impl GenSvc {
    /*
    pub async fn insert<'a, A, B: 'a, FA>(
        validate: impl FnOnce(&'a A) -> Option<anyhow::Error>,
        map_dto: impl FnOnce(&'a A) -> &'a B,
        find_unique: impl FnOnce(&'a B) -> FA,
        insert: impl FnOnce(&'a B) -> FA,
        dto: &'a A,
    ) -> Option<anyhow::Error>
    where
        FA: Future<Output = Option<anyhow::Error>>,
    {
        validate(dto)?;
        let item = map_dto(dto);
        find_unique(&item).await?;
        insert(&item).await
    }
    */

    pub async fn insert<A: Clone, B: Clone, FA>(
        validate: impl FnOnce(A) -> Option<anyhow::Error>,
        map_dto: impl FnOnce(A) -> B,
        find_unique: impl FnOnce(B) -> FA,
        check_associations: impl FnOnce(B) -> FA,
        insert: impl FnOnce(B) -> FA,
        dto: A,
    ) -> Option<anyhow::Error>
    where
        FA: Future<Output = Option<anyhow::Error>>,
    {
        validate(dto.clone())?;
        let item = map_dto(dto.clone());
        find_unique(item.clone()).await?;
        check_associations(item.clone()).await?;
        insert(item.clone()).await
    }

    pub async fn update<A: Clone, B: Clone, FA>(
        validate: impl FnOnce(A) -> Option<Error>,
        map_to_entity: impl FnOnce(A) -> B,
        find_unique: impl FnOnce(B) -> FA,
        check_associations: impl FnOnce(B) -> FA,
        update: impl FnOnce(B) -> FA,
        dto: A,
    ) -> Option<Error>
    where
        FA: Future<Output = Option<Error>>,
    {
        validate(dto.clone())?;
        let entity = map_to_entity(dto.clone());
        find_unique(entity.clone()).await?;
        check_associations(entity.clone()).await?;
        update(entity).await
    }

    pub async fn save<'a, A, B: 'a, FA>(
        validate: impl FnOnce(&'a A) -> Option<anyhow::Error>,
        map_dto: impl FnOnce(&'a A) -> &'a B,
        find_unique: impl FnOnce(&'a B) -> FA,
        save: impl FnOnce(&'a B) -> FA,
        item: &'a A,
    ) -> Option<anyhow::Error>
    where
        FA: Future<Output = Option<anyhow::Error>>,
    {
        validate(item)?;
        let entity = map_dto(item);
        find_unique(entity).await?;
        save(entity).await
    }

    pub async fn save_<'a, A, FA>(
        validate: impl Fn(&'a A) -> Option<anyhow::Error>,
        find_unique: impl Fn(&'a A) -> FA,
        save: impl Fn(&'a A) -> FA,
        item: &'a A,
    ) -> Option<anyhow::Error>
    where
        FA: Future<Output = Option<anyhow::Error>>,
    {
        find_unique(&item).await?;
        save(&item).await
    }

    pub async fn save_again<A, FA>(find_unique: impl FnOnce(&A) -> FA, item: &A) -> Option<Error>
    where
        FA: Future<Output = Option<Error>>,
    {
        find_unique(item).await
    }

    pub async fn delete<A, FA>(
        check_associations: impl FnOnce(&A) -> FA,
        delete: impl FnOnce(&A) -> FA,
        item: &A,
    ) -> Option<Error>
    where
        FA: Future<Output = Option<Error>>,
    {
        check_associations(item).await?;
        delete(item).await
    }

    pub async fn remove<ID: Clone, A: Clone, FA, FB>(
        find_by_id: impl FnOnce(ID) -> FA,
        check_associations: impl FnOnce(A) -> FB,
        delete: impl FnOnce(A) -> FB,
        id: ID,
    ) -> Option<Error>
    where
        FA: Future<Output = Result<Option<A>, anyhow::Error>>,
        FB: Future<Output = Option<Error>>,
    {
        match find_by_id(id.clone()).await {
            Err(e) => Some(e),
            Ok(None) => Some(anyhow!("Entity not found")),
            Ok(Some(entity)) => {
                check_associations(entity.clone()).await?;
                delete(entity.clone()).await
            }
        }
    }

    pub async fn find_by_id<ID, A>(
        find_by_id: impl FnOnce(&ID) -> Option<A>,
        id: &ID,
    ) -> Option<A> {
        find_by_id(id)
    }

    pub async fn find_by_conds<C, A>(
        find_by_conds: impl FnOnce(&[&C]) -> Option<A>,
        conds: &[&C],
    ) -> Option<A> {
        find_by_conds(conds)
    }

    pub async fn fetch_by_conds<C, A, FA>(
        find_by_conds: impl FnOnce(&[&C]) -> FA,
        conds: &[&C],
    ) -> Vec<A>
    where
        FA: Future<Output = Vec<A>>,
    {
        find_by_conds(conds).await
    }

    pub async fn fetch_map_by_conds<C, A, K, FA>(
        find_by_conds: impl FnOnce(&[&C]) -> FA,
        conds: &[&C],
    ) -> HashMap<K, A>
    where
        FA: Future<Output = HashMap<K, A>>,
    {
        find_by_conds(conds).await
    }
}
