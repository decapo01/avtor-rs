use serde::Deserialize;

#[derive(Deserialize, Debug, Clone)]
pub struct EnvConfig {
    pub port: u16,
    pub db_host: String,
    pub db_port: String,
    pub db_user: String,
    pub db_pass: String,
    pub db_name: Option<String>,
    pub main_account_id: uuid::Uuid,
    pub main_account_name: String,
    pub super_user_id: uuid::Uuid,
    pub super_user_username: String,
    pub super_user_password: String,
    pub app_secret: String,
}
