use argon2::Config as ArgonConfig;
use rand::Rng;

pub fn hash_password(input: &String) -> Result<String, anyhow::Error> {
    let salt: [u8; 32] = rand::thread_rng().gen();
    let config = ArgonConfig::default();
    let hash = argon2::hash_encoded(input.as_bytes(), &salt, &config)?;
    Ok(hash)
}
