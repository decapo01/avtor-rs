use actix_web::{
    web::{self, Data},
    HttpRequest, HttpResponse,
};
use deadpool_postgres::{Pool, Transaction};
use futures::{future::BoxFuture, Future};
use jwt_simple::prelude::{HS256Key, MACLike};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

use self::handler_data::HandlerData;

pub mod env_config;
pub mod handler_data;
pub mod hash_password;
pub mod service;

#[derive(Serialize, Deserialize, Clone)]
pub struct LoggedInUser {
    pub id: Uuid,
    pub username: String,
    pub roles: Vec<String>,
    pub account_id: Uuid,
}

type AuthToken = String;

pub fn decode_auth_token(secret: &str, token: AuthToken) -> Result<LoggedInUser, anyhow::Error> {
    let key = HS256Key::from_bytes(secret.as_bytes());
    let claims = key.verify_token::<LoggedInUser>(&token, None)?;
    Ok(claims.custom)
}

pub async fn with_trans<F, T>(pool: Pool, f: F) -> T
where
    F: for<'a> FnOnce(&'a Transaction<'a>) -> BoxFuture<'a, T>,
{
    // todo: remove unwraps
    let mut conn = pool.get().await.unwrap();
    let trans = conn.transaction().await.unwrap();
    let res = f(&trans).await;
    trans.commit().await;
    res
}

pub async fn with_trans_res<F, T>(pool: Pool, f: F) -> Result<T, anyhow::Error>
where
    F: for<'a> FnOnce(&'a Transaction<'a>) -> BoxFuture<'a, T>,
{
    // todo: remove unwraps
    let mut conn = pool.get().await.map_err(anyhow::Error::from)?;
    let trans = conn.transaction().await.map_err(anyhow::Error::from)?;
    let res = f(&trans).await;
    trans.commit().await.map_err(anyhow::Error::from)?;
    Ok(res)
}

pub async fn with_user<FA>(
    req: HttpRequest,
    data: Data<HandlerData>,
    roles: Vec<String>,
    f: impl FnOnce(LoggedInUser) -> FA,
) -> HttpResponse
where
    FA: Future<Output = HttpResponse>,
{
    let auth_cookie = req.cookie("sid");
    match auth_cookie {
        None => HttpResponse::Forbidden().body("Forbidden"),
        Some(c) => {
            let x = c.value().to_string();
            let logged_in_user_res = decode_auth_token(data.clone().secret.as_str(), x);
            match logged_in_user_res {
                Err(_) => HttpResponse::Forbidden().body("Forbidden"),
                Ok(logged_in_user) => {
                    if roles.iter().any(|r| logged_in_user.roles.contains(r)) {
                        f(logged_in_user).await
                    } else {
                        HttpResponse::Unauthorized().body("Unauthorized")
                    }
                }
            }
        }
    }
}

pub async fn with_user_opt<F>(
    req: HttpRequest,
    data: web::Data<HandlerData>,
    roles: Vec<String>,
    f: impl FnOnce(Option<LoggedInUser>) -> F,
) -> HttpResponse
where
    F: Future<Output = HttpResponse>,
{
    let auth_cookie = req.cookie("sid");
    match auth_cookie {
        None => f(None).await,
        Some(c) => {
            let x = c.value().to_string();
            let logged_in_user_res = decode_auth_token(data.clone().secret.as_str(), x);
            match logged_in_user_res {
                Err(_) => f(None).await,
                Ok(logged_in_user) => {
                    if roles.iter().any(|x| logged_in_user.roles.contains(x)) {
                        f(Some(logged_in_user.clone())).await
                    } else {
                        HttpResponse::Unauthorized().body("Unauthorized")
                    }
                }
            }
        }
    }
}

pub fn with_user_and_transaction<FA>(
    req: HttpRequest,
    data: Data<HandlerData>,
    roles: Vec<String>,
    f: FA,
) -> impl Future<Output = HttpResponse>
where
    FA: for<'a> FnOnce(LoggedInUser, &'a Transaction) -> BoxFuture<'a, HttpResponse>,
{
    with_user(req.clone(), data.clone(), roles.clone(), move |user| {
        with_trans(data.clone().pool.clone(), move |trans| {
            f(user.clone(), trans)
        })
    })
}

pub fn with_user_opt_and_transaction<FA>(
    req: HttpRequest,
    data: Data<HandlerData>,
    roles: Vec<String>,
    f: FA,
) -> impl Future<Output = HttpResponse>
where
    FA: for<'a> FnOnce(Option<LoggedInUser>, &'a Transaction) -> BoxFuture<'a, HttpResponse>,
{
    with_user_opt(req.clone(), data.clone(), roles.clone(), move |user| {
        with_trans(data.clone().pool.clone(), move |trans| {
            f(user.clone(), trans)
        })
    })
}
