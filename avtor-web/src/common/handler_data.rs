use deadpool_postgres::Pool;

#[derive(Clone)]
pub struct HandlerData {
    pub pool: Pool,
    pub secret: String,
    pub env: String,
}
