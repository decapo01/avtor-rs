use actix_web::{
    cookie::Cookie,
    web::{Data, Form},
    HttpResponse, Responder,
};
use avtor_core::models::{
    accounts::AccountId,
    dtos::LoginDto,
    users::{User, UserId},
    LoggedInUser, LoginError, LoginRepoError,
};
use futures::{future::BoxFuture, TryFutureExt};
use jwt_simple::prelude::{Claims, Duration, HS256Key, MACLike};
use tokio_postgres::Transaction;

use crate::{
    common::with_trans,
    repo::rows::{UserRowCriteria, UserRowRepo},
    services::account_service::AccountService,
    HandlerData, Secret,
};

// _todo: put somewhere else
pub fn find_user_by_username<'a>(
    trans: &'a Transaction<'a>,
) -> impl FnOnce(String) -> BoxFuture<'a, Result<Option<User>, LoginRepoError>> {
    move |name: String| {
        Box::pin(async move {
            let urr = UserRowRepo::find_by_criteria(trans)(vec![UserRowCriteria::UsernameEq(name)])
                .map_err(|e| LoginRepoError(e.to_string()))
                .await;
            urr.map(|uro| {
                uro.map(|ur| User {
                    id: UserId(ur.id),
                    username: ur.username,
                    password: ur.password,
                    roles: ur.roles,
                    account_id: AccountId(ur.account_id),
                    locked: ur.locked,
                    active: ur.active,
                })
            })
        })
    }
}

pub fn verify_password(hash: &String, pw: &String) -> Result<bool, LoginError> {
    let r = argon2::verify_encoded(&hash, &pw.as_bytes())
        .map_err(|_| LoginError::PasswordDoesNotMatch)?;
    Ok(r)
}

pub type AuthToken = String;

pub fn create_auth_token(
    key_id: Secret,
    logged_in_user: LoggedInUser,
) -> Result<AuthToken, LoginError> {
    let key = HS256Key::from_bytes(key_id.as_bytes());
    let claims = Claims::with_custom_claims(logged_in_user, Duration::from_hours(3));
    let token = key.authenticate(claims).unwrap();
    Ok(token)
}

pub async fn post_login(data: Data<HandlerData>, form: Form<LoginDto>) -> impl Responder {
    let dto = form.0;
    with_trans(data.pool.clone(), move |trans| {
        Box::pin(async move {
            let res = AccountService::login(
                find_user_by_username(&trans),
                verify_password,
                create_auth_token,
                dto,
                data.secret.clone(),
            )
            .await;
            match res {
                Err(_) => HttpResponse::Ok().body("login bad"),
                Ok(token) => {
                    let secure_cookie = if data.env == "local" { false } else { true };
                    let cookie = Cookie::build("sid", token)
                        .secure(secure_cookie)
                        .http_only(true)
                        .finish();
                    HttpResponse::Found()
                        .append_header(("location", "/"))
                        .cookie(cookie)
                        .finish()
                }
            }
        })
    })
    .await
}
