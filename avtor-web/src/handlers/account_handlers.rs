use actix_web::{
    web::{Data, Json, Path},
    HttpRequest, HttpResponse,
};
use anyhow::anyhow;
use avtor_core::models::accounts::{Account, AccountDto, AccountId};
use futures::future::BoxFuture;
use tokio_postgres::{GenericClient, Transaction};
use uuid::Uuid;
use validator::Validate;

use crate::{
    common::{handler_data::HandlerData, service::GenSvc, with_user_and_transaction},
    postgres_common::postgres_utils::delete,
    repo::{
        account_row_repo::{
            entity_from_row, row_from_entity, AccountFindUniqueOp, AccountRowRepoFuncs,
        },
        rows::{AccountRowCriteria, AccountRowRepo},
    },
};
use crate::common::with_trans;
use crate::postgres_common::postgres_utils::quick_func;
use crate::repo::app_account_view_model_repo::do_something;

pub fn validate(dto: AccountDto) -> Option<anyhow::Error> {
    match dto.validate() {
        Err(e) => Some(e.into()),
        Ok(_) => None,
    }
}

pub fn dto_from_account(account: Account) -> AccountDto {
    AccountDto {
        id: account.id.0,
        name: account.name,
    }
}

pub fn account_from_dto(dto: AccountDto) -> Account {
    Account {
        id: AccountId(dto.id),
        name: dto.name,
    }
}

//todo: move this somewhere else
pub fn check_associations<'a>(
    _trans: &'a Transaction<'a>,
) -> impl FnOnce(Account) -> BoxFuture<'a, Option<anyhow::Error>> {
    move |_account| Box::pin(async move { None })
}

pub fn find_by_id<'a>(
    trans: &'a Transaction<'a>,
) -> impl FnOnce(Uuid) -> BoxFuture<'a, Result<Option<Account>, anyhow::Error>> {
    move |id| {
        Box::pin(async move {
            let conds = vec![AccountRowCriteria::IdEq(id)];
            AccountRowRepo::find_by_criteria(trans)(conds)
                .await
                .map(|r| r.map(entity_from_row))
                .map_err(|e| anyhow!("{}", e.to_string()))
        })
    }
}

pub fn delete_account<'a>(
    trans: &'a Transaction<'a>,
) -> impl FnOnce(Account) -> BoxFuture<'a, Option<anyhow::Error>> {
    move |account| {
        Box::pin(async move {
            let row = row_from_entity(account);
            let res = AccountRowRepo::delete(trans)(&row).await;
            match res {
                Ok(_) => None,
                Err(e) => Some(e),
            }
        })
    }
}

pub async fn post_create(
    req: HttpRequest,
    data: Data<HandlerData>,
    Json(account_dto): Json<AccountDto>,
) -> HttpResponse {
    with_user_and_transaction(req, data.clone(), vec![], move |_user, trans| {
        Box::pin(async move {
            let res = GenSvc::insert(
                validate,
                account_from_dto,
                AccountRowRepoFuncs::find_unique(trans, AccountFindUniqueOp::Create),
                check_associations(trans),
                AccountRowRepoFuncs::insert(trans),
                account_dto,
            )
            .await;
            match res {
                Some(e) => HttpResponse::BadRequest().body(e.to_string()),
                None => HttpResponse::Ok().body("success"),
            }
        })
    })
    .await
}

pub async fn post_update(
    req: HttpRequest,
    data: Data<HandlerData>,
    Json(account_dto): Json<AccountDto>,
) -> HttpResponse {
    with_user_and_transaction(req, data.clone(), vec![], move |_user, trans| {
        Box::pin(async move {
            let res = GenSvc::update(
                validate,
                account_from_dto,
                AccountRowRepoFuncs::find_unique(trans, AccountFindUniqueOp::Create),
                check_associations(trans),
                AccountRowRepoFuncs::insert(trans),
                account_dto,
            )
            .await;
            match res {
                Some(e) => HttpResponse::BadRequest().body(e.to_string()),
                None => HttpResponse::Ok().body("success"),
            }
        })
    })
    .await
}

pub async fn post_remove(
    req: HttpRequest,
    data: Data<HandlerData>,
    path: Path<uuid::Uuid>,
) -> HttpResponse {
    with_user_and_transaction(req, data.clone(), vec![], move |_user, trans| {
        Box::pin(async move {
            let id = path.into_inner();
            let res = GenSvc::remove(
                find_by_id(trans),
                check_associations(trans),
                delete_account(trans),
                id,
            )
            .await;
            match res {
                Some(e) => HttpResponse::BadRequest().body(e.to_string()),
                None => HttpResponse::Ok().body("success"),
            }
        })
    })
    .await
}


pub async fn get_something(
    _req: HttpRequest,
    data: Data<HandlerData>,
) -> HttpResponse {
    let mut mgr = data.pool.get().await.unwrap();
    let client = mgr.client();
    let rows = quick_func(client, vec![Box::new(1)]).await;
    let res : Vec<String> = rows.iter().map(|r| r.get("")).collect();
    HttpResponse::Ok().json(res)
}