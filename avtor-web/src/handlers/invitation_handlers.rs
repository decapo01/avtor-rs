use std::str::FromStr;

use actix_web::{
    web::{Data, Form},
    HttpRequest, HttpResponse,
};
use avtor_core::models::invitations::{Invitation, InvitationDto};
use chrono::{DateTime, Utc};
use uuid::Uuid;

use crate::{
    common::{decode_auth_token, service::GenSvc, with_trans, LoggedInUser},
    repo::invitation_repo::{InvitationQueries, InvitationSaveOp},
    HandlerData,
};

pub enum AuthRes {
    Missing(HttpResponse),
    Auth(LoggedInUser),
}

pub fn get_auth_user(req: &HttpRequest, data: &Data<HandlerData>, roles: Vec<String>) -> AuthRes {
    let auth_cookie = req.cookie("sid");
    match auth_cookie {
        None => AuthRes::Missing(HttpResponse::Forbidden().body("Forbidden")),
        Some(c) => {
            let x = c.value().to_string();
            let logged_in_user_res = decode_auth_token(data.clone().secret.as_str(), x);
            match logged_in_user_res {
                Err(_) => AuthRes::Missing(HttpResponse::Forbidden().body("Forbidden")),
                Ok(logged_in_user) => {
                    if roles.iter().any(|r| logged_in_user.roles.contains(r)) {
                        AuthRes::Auth(logged_in_user)
                    } else {
                        AuthRes::Missing(HttpResponse::Unauthorized().body("Unauthorized"))
                    }
                }
            }
        }
    }
}

//todo: move this to another area
pub fn invitation_from_dto<'a>(
    roles: &'a Vec<String>,
    expiration_date: &'a DateTime<Utc>,
    accepted_on: &'a Option<DateTime<Utc>>,
) -> impl FnOnce(&'a InvitationDto) -> Invitation {
    move |dto: &InvitationDto| Invitation {
        id: Uuid::from_str(dto.id.as_str()).unwrap(),
        username: dto.email.clone(),
        roles: roles.clone(),
        account_id: dto.account_id.clone(),
        expiration_date: expiration_date.clone(),
        accepted_on: accepted_on.clone(),
    }
}

pub async fn post_create(
    req: HttpRequest,
    data: Data<HandlerData>,
    Form(invitation): Form<Invitation>,
) -> HttpResponse {
    match get_auth_user(&req, &data, vec![]) {
        AuthRes::Missing(_) => HttpResponse::Unauthorized().body("Unauthorized"),
        AuthRes::Auth(_) => {
            with_trans(data.pool.clone(), move |trans| {
                Box::pin(async move {
                    let r = GenSvc::save_(
                        |_| None,
                        InvitationQueries::find_unique(
                            trans,
                            &InvitationSaveOp::Insert {
                                username_eq: invitation.username.clone(),
                            },
                        ),
                        InvitationQueries::insert(trans),
                        &invitation,
                    )
                    .await;
                    match r {
                        // todo: update msg
                        Some(_e) => HttpResponse::BadRequest().body("bad req"),
                        None => HttpResponse::Ok().body("inv created"),
                    }
                })
            })
            .await
        }
    }
}
