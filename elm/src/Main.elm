module Main exposing (..)

import Element exposing (..)
import Html exposing (Html)
import Html.Attributes exposing (hidden)


type alias Model =
    {}


type Msg
    = NoOp


elText : String -> Element msg
elText =
    Element.text



-- view : Element msg
-- view =
--     el [ centerX ] (elText "Hello")


navRow : Element msg
navRow =
    row [ width fill ]
        [ el [ alignLeft ] (text "Avtor")
        , el [ alignRight ] (text "Notifications")
        ]


main : Html msg
main =
    layout [ padding 50 ] <|
        column [ width fill ]
            [ navRow
            ]
