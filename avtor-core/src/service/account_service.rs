use std::{collections::HashMap, future::Future};

use anyhow::Error;
use serde::{Deserialize, Serialize};
use uuid::Uuid;
use validator::{Validate, ValidationErrors};

use crate::models::{
    accounts::{Account, AccountId},
    users::{AccountDto, CreateAccountError, CreateSuperUserError, User, UserDto},
};

/*
#[derive(Serialize, Deserialize, Clone)]
pub struct LoggedInUser {
    pub id: Uuid,
    pub username: String,
    pub roles: Vec<String>,
    pub account_id: Uuid,
}
*/

// todo: move somewhere
fn hash_map_from_validation_errors(e: ValidationErrors) -> HashMap<String, String> {
    let field_errors = e.field_errors();
    field_errors
        .into_iter()
        .map(|fe| {
            let (field, msgs) = fe;
            let msg = msgs.into_iter().fold("".to_string(), |acc, x| {
                if acc.is_empty() {
                    x.to_string()
                } else {
                    format!("{}, {}", acc, x.to_string())
                }
            });
            (field.to_string(), msg)
        })
        .collect()
}

type Secret = String;
type AuthToken = String;

/*
pub struct AccountService {}

impl AccountService {
    pub async fn create_super_user<FA, FB, FC, FD>(
        find_super_user: impl Fn() -> FA,
        insert: impl FnOnce(User) -> FB,
        insert_account: impl FnOnce(Account) -> FC,
        find_account_by_id: impl FnOnce(Uuid) -> FD,
        user_dto: &UserDto,
        account_dto: &AccountDto,
    ) -> Result<(), CreateSuperUserError>
    where
        FA: Future<Output = Result<Option<User>, CreateSuperUserError>>,
        FB: Future<Output = Result<(), CreateSuperUserError>>,
        FC: Future<Output = Result<(), CreateAccountError>>,
        FD: Future<Output = Result<Option<Account>, CreateAccountError>>,
    {
        let _ = user_dto.validate().map_err(|e| {
            let hash_map = hash_map_from_validation_errors(e);
            CreateSuperUserError::UserInvalid(hash_map)
        })?;
        let _ = account_dto.validate().map_err(|e| {
            let hash_map = hash_map_from_validation_errors(e);
            CreateSuperUserError::AccountInvalid(hash_map)
        })?;
        let user = user_from_dto(user_dto.clone());
        let maybe_existing_user = find_super_user().await?;
        match maybe_existing_user {
            Some(_) => Err(CreateSuperUserError::SuperUserExists),
            None => {
                let maybe_existing_account = find_account_by_id(account_dto.id)
                    .await
                    .map_err(|e| CreateSuperUserError::RepoError(e.to_string()))?;
                match maybe_existing_account {
                    Some(_) => Err(CreateSuperUserError::AccountExists),
                    None => {
                        let account = Account {
                            id: AccountId(account_dto.id),
                            name: account_dto.clone().name,
                        };
                        let _ = insert_account(account)
                            .await
                            .map_err(|e| CreateSuperUserError::RepoError(e.to_string()))?;
                        let ins_res = insert(user).await;
                        match ins_res {
                            Ok(_) => Ok(()),
                            Err(e) => Err(e),
                        }
                    }
                }
            }
        }
    }

    pub fn login<'a, FA: Send + Future<Output = Result<Option<User>, Error>>>(
        find_user_by_username: impl FnOnce(String) -> FA,
        verify_password: impl FnOnce(&String, &String) -> Result<bool, Error>,
        create_auth_token: impl FnOnce(Secret, LoggedInUser) -> Result<AuthToken, Error>,
        // ) -> impl FnOnce(LoginDto, Secret) -> BoxFuture<'a, Result<AuthToken, Error>>  {
    ) -> String {
        todo!()
    }
}
*/
