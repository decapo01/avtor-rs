use std::collections::HashMap;

pub fn hash_map_to_string(hash_map: HashMap<String, String>) -> String {
    hash_map
        .into_iter()
        .fold("".to_string(), |acc, (key, val)| {
            let new_msg = format!("{key}: {val}");
            if acc.is_empty() {
                new_msg
            } else {
                format!("{acc}, {new_msg}")
            }
        })
}

pub fn string_from_hash_map(hash_map: HashMap<String, String>) -> String {
    hash_map_to_string(hash_map)
}
