use std::collections::HashMap;

use anyhow::Error;
// use futures::Future;

/*
pub struct GenSvc;

impl GenSvc {
    pub async fn insert<A, FA>(
        validate: impl FnOnce(&A) -> Option<Error>,
        find_unique: impl FnOnce(&A) -> FA,
        insert: impl FnOnce(&A) -> FA,
        item: &A,
    ) -> Option<Error>
    where
        FA: Future<Output = Option<Error>>,
    {
        validate(item)?;
        find_unique(item).await?;
        insert(item).await
    }

    pub async fn update<A, FA>(
        validate: impl FnOnce(&A) -> Option<Error>,
        find_unique: impl FnOnce(&A) -> FA,
        update: impl FnOnce(&A) -> FA,
        item: &A,
    ) -> Option<Error>
    where
        FA: Future<Output = Option<Error>>,
    {
        validate(item)?;
        find_unique(item).await?;
        update(item).await
    }

    pub async fn save<'a, A, FA>(
        validate: impl FnOnce(&'a A) -> Option<Error>,
        find_unique: impl FnOnce(&'a A) -> FA,
        check_associations: impl FnOnce(&'a A) -> FA,
        save: impl FnOnce(&'a A) -> FA,
        item: &'a A,
    ) -> Option<Error>
    where
        FA: Future<Output = Option<Error>>,
    {
        validate(item)?;
        find_unique(item).await?;
        check_associations(item).await?;
        save(item).await
    }

    pub async fn save_again<A, FA>(find_unique: impl FnOnce(&A) -> FA, item: &A) -> Option<Error>
    where
        FA: Future<Output = Option<Error>>,
    {
        find_unique(item).await
    }

    pub async fn delete<A, FA>(
        check_associations: impl FnOnce(&A) -> FA,
        delete: impl FnOnce(&A) -> FA,
        item: &A,
    ) -> Option<Error>
    where
        FA: Future<Output = Option<Error>>,
    {
        check_associations(item).await?;
        delete(item).await
    }

    pub async fn find_by_id<ID, A>(
        find_by_id: impl FnOnce(&ID) -> Option<A>,
        id: &ID,
    ) -> Option<A> {
        find_by_id(id)
    }

    pub async fn find_by_conds<C, A>(
        find_by_conds: impl FnOnce(&[&C]) -> Option<A>,
        conds: &[&C],
    ) -> Option<A> {
        find_by_conds(conds)
    }

    pub async fn fetch_by_conds<C, A, FA>(
        find_by_conds: impl FnOnce(&[&C]) -> FA,
        conds: &[&C],
    ) -> Vec<A>
    where
        FA: Future<Output = Vec<A>>,
    {
        find_by_conds(conds).await
    }

    pub async fn fetch_map_by_conds<C, A, K, FA>(
        find_by_conds: impl FnOnce(&[&C]) -> FA,
        conds: &[&C],
    ) -> HashMap<K, A>
    where
        FA: Future<Output = HashMap<K, A>>,
    {
        find_by_conds(conds).await
    }
}
*/
