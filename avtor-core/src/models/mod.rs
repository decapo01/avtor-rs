use serde::{Deserialize, Serialize};
use uuid::Uuid;

use self::users::User;

pub mod accounts;
pub mod app_view_models;
pub mod apps;
pub mod blocked_ip;
pub mod common;
pub mod config;
pub mod dtos;
pub mod internal_users;
pub mod invitations;
pub mod login_attempts;
pub mod users;
pub mod view_models;

pub struct LoginRepoError(pub String);

pub enum LoginError {
    UserNotFound,
    PasswordDoesNotMatch,
    RepoError(LoginRepoError),
}

#[derive(Serialize, Deserialize, Clone)]
pub struct LoggedInUser {
    pub id: Uuid,
    pub username: String,
    pub roles: Vec<String>,
    pub account_id: Uuid,
}

impl From<User> for LoggedInUser {
    fn from(value: User) -> Self {
        LoggedInUser {
            id: value.id.0,
            username: value.username,
            roles: value
                .roles
                .split(",")
                .map(|x| x.to_string())
                .collect::<Vec<String>>(),
            account_id: value.account_id.0,
        }
    }
}
