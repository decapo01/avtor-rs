use chrono::{DateTime, Utc};
// use futures::Future;
use serde::{Deserialize, Serialize};
use uuid::Uuid;
use validator::Validate;

#[derive(Serialize, Deserialize, Clone)]
pub struct Invitation {
    pub id: Uuid,
    pub username: String,
    pub roles: Vec<String>,
    pub account_id: Uuid,
    pub expiration_date: DateTime<Utc>,
    pub accepted_on: Option<DateTime<Utc>>,
}

#[derive(Serialize, Deserialize, Validate, Debug)]
pub struct InvitationDto {
    // make uuid validator
    #[validate(length(min = 1))]
    pub id: String,
    #[validate(email)]
    pub email: String,
    pub account_id: Uuid,
}

// use crate::common::service::GenSvc;

// pub struct InvitationService;
// impl InvitationService {
//     pub async fn save<FA>(
//         validate: impl FnOnce(&Invitation) -> Option<Error>,
//         find_unique: impl FnOnce(&Invitation) -> FA,
//         check_associations: impl FnOnce(&Invitation) -> FA,
//         save: impl FnOnce(&Invitation) -> FA,
//         item: &Invitation,
//     ) -> Option<Error>
//     where
//         FA: Future<Output = Option<Error>>,
//     {
//         GenSvc::save(validate, find_unique, check_associations, save, item).await
//     }
// }
