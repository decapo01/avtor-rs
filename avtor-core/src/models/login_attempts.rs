use chrono::{DateTime, Utc};
use uuid::Uuid;

pub enum LoginAttemptError {
    UsernameNotFound,
    PasswordDoesNotMatch,
}

pub struct LoginAttempt {
    pub id: Uuid,
    pub ip: String,
    pub attempted_on: DateTime<Utc>,
    pub login_attempt_error: Option<LoginAttemptError>,
    pub username: String,
}
