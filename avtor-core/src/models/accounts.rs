use serde::{Deserialize, Serialize};
use uuid::Uuid;
use validator::Validate;

#[derive(Debug, Clone, Serialize, Deserialize, Default, PartialEq)]
// pub struct AccountId(pub String);
pub struct AccountId(pub Uuid);

#[derive(Clone, Debug, Default, Serialize, Deserialize, PartialEq)]
pub struct Account {
    pub id: AccountId,
    pub name: String,
}

#[derive(Clone, Debug, Default, Serialize, Deserialize, Validate)]
pub struct AccountDto {
    pub id: Uuid,
    #[validate(length(min = 3, max = 255))]
    pub name: String,
}

impl From<AccountDto> for Account {
    fn from(value: AccountDto) -> Self {
        Account { id: AccountId(value.id), name: value.name }
    }
}

impl From<Account> for AccountDto {
    fn from(value: Account) -> Self {
        AccountDto { id: value.id.0, name: value.name }
    }
}

impl Account {
    pub fn to_dto(self) -> AccountDto {
        AccountDto { id: self.id.0, name: self.name }
    }
}

impl AccountDto {
    pub fn to_entity(self) -> Account {
        Account { id: AccountId(self.id), name: self.name }
    }
}