use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Serialize, Deserialize, Debug, Default)]
pub struct InternalUser {
    pub id: Uuid,
    pub username: String,
    pub password: String,
    pub roles: Vec<String>,
    pub is_active: bool,
}
