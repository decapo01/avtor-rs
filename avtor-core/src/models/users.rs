use serde::{Deserialize, Serialize};
use std::{
    collections::HashMap,
    fmt::{self, Display},
    future::Future,
};
use uuid::Uuid;
use validator::{Validate, ValidationError, ValidationErrors};

use super::accounts::{Account, AccountId};
#[derive(Debug, Clone, Copy, Serialize, Deserialize, Default)]
pub struct UserId(pub Uuid);

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct User {
    pub id: UserId,
    pub username: String,
    pub password: String,
    pub roles: String,
    pub account_id: AccountId,
    pub locked: bool,
    pub active: bool,
}

pub enum Roles {
    SuperUser,
    Admin,
    User,
}

impl Roles {
    pub fn to_snake_string(self) -> String {
        match self {
            Roles::SuperUser => "super_user".to_string(),
            Roles::Admin => "admin".to_string(),
            Roles::User => "user".to_string(),
        }
    }
}

#[derive(Debug, Validate, Deserialize, Clone)]
pub struct UserDto {
    pub id: Uuid,
    #[validate(length(min = 3, message = "username_required"))]
    pub username: String,
    #[validate(length(min = 8, max = 18, message = "password_between_8_and_18_chars"))]
    pub password: String,
    #[validate(length(min = 1, message = "roles_required"))]
    pub roles: String,
    pub account_id: Uuid,
}

impl From<UserDto> for User {
    fn from(value: UserDto) -> Self {
        User {
            id: UserId(value.id),
            username: value.username,
            password: value.password,
            roles: value.roles,
            account_id: AccountId(value.account_id),
            locked: false,
            active: true,
        }
    }
}

impl From<User> for UserDto {
    fn from(value: User) -> Self {
        UserDto {
            id: value.id.0,
            username: value.username,
            password: value.password,
            roles: value.roles,
            account_id: value.account_id.0,
        }
    }
}

#[derive(Debug, thiserror::Error)]
pub enum UserValidationError {
    #[error("Username invalid: {0}")]
    UsernameInvalid(String),
    #[error("Password invalid: {0}")]
    PasswordInvalid(String),
    #[error("Roles invalid: {0}")]
    RolesInvalid(String),
}

#[derive(Debug, thiserror::Error)]
pub struct UserValidationErrors {
    username: Option<String>,
    password: Option<String>,
    roles: Option<String>,
}

impl Display for UserValidationErrors {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let msg = vec![
            self.username.as_ref(),
            self.password.as_ref(),
            self.roles.as_ref(),
        ]
        .into_iter()
        .fold("".to_string(), |acc, x| {
            let msg = match x {
                Some(m) => m,
                None => "",
            }
            .to_owned();
            if acc.is_empty() {
                msg
            } else {
                format!("{acc}, {msg}")
            }
        });
        write!(f, "{}", msg)
    }
}

#[derive(Debug, thiserror::Error)]
pub enum CreateSuperUserError {
    #[error("User invalid")]
    UserInvalid(HashMap<String, String>),

    #[error("Only one super user can exist on a system")]
    SuperUserExists,

    #[error("Repo Error: {0}")]
    RepoError(String),

    #[error("An unknown error occurred")]
    UnknownError,

    #[error("Account invalid")]
    AccountInvalid(HashMap<String, String>),

    #[error("Account exits")]
    AccountExists,
}

impl From<anyhow::Error> for CreateSuperUserError {
    fn from(_: anyhow::Error) -> Self {
        CreateSuperUserError::UnknownError
    }
}

// impl Into<anyhow::Error> for CreateSuperUserError {
//     fn into(self) -> anyhow::Error {
//         anyhow!(self.to_string())
//     }
// }

fn hash_map_from_validation_errors(e: ValidationErrors) -> HashMap<String, String> {
    let field_errors = e.field_errors();
    field_errors
        .into_iter()
        .map(|fe| {
            let (field, msgs) = fe;
            let msg = msgs.into_iter().fold("".to_string(), |acc, x| {
                if acc.is_empty() {
                    x.to_string()
                } else {
                    format!("{acc}, {x}")
                }
            });
            (field.to_string(), msg)
        })
        .collect()
}

pub fn concat_validation_errors(v_errs_opt: Option<&&Vec<ValidationError>>) -> String {
    match v_errs_opt {
        None => "".to_string(),
        Some(v_errs) => v_errs.into_iter().fold("".to_string(), |acc, x| {
            if acc.is_empty() {
                match &x.message {
                    Some(m) => m.to_string(),
                    None => "".to_string(),
                }
            } else {
                format!("{acc}, {x}")
            }
        }),
    }
}

// todo: move this with the user dto
#[derive(Serialize, Deserialize, Validate, Clone)]
pub struct AccountDto {
    pub id: Uuid,
    // pub id: String,
    pub name: String,
}

pub async fn create_super_user<FA, FB, FC, FD>(
    find_super_user: impl FnOnce() -> FA,
    insert: impl FnOnce(User) -> FB,
    insert_account: impl FnOnce(Account) -> FC,
    find_account_by_id: impl FnOnce(AccountId) -> FD,
    user_dto: &UserDto,
    account_dto: &AccountDto,
) -> Result<(), CreateSuperUserError>
where
    FA: Future<Output = Result<Option<User>, CreateSuperUserError>>,
    FB: Future<Output = Result<(), CreateSuperUserError>>,
    FC: Future<Output = Result<(), CreateAccountError>>,
    FD: Future<Output = Result<Option<Account>, CreateAccountError>>,
{
    let _ = user_dto.validate().map_err(|e| {
        let hash_map = hash_map_from_validation_errors(e);
        CreateSuperUserError::UserInvalid(hash_map)
    })?;
    let _ = account_dto.validate().map_err(|e| {
        let hash_map = hash_map_from_validation_errors(e);
        CreateSuperUserError::AccountInvalid(hash_map)
    })?;
    let user: User = User::from(user_dto.clone());
    let maybe_existing_user = find_super_user().await?;
    match maybe_existing_user {
        Some(_) => Err(CreateSuperUserError::SuperUserExists),
        None => {
            let maybe_existing_account = find_account_by_id(AccountId(account_dto.id.clone()))
                .await
                .map_err(|e| CreateSuperUserError::RepoError(e.to_string()))?;
            match maybe_existing_account {
                Some(_) => Err(CreateSuperUserError::AccountExists),
                None => {
                    let account = Account {
                        id: AccountId(account_dto.id.clone()),
                        name: account_dto.clone().name,
                    };
                    let _ = insert_account(account)
                        .await
                        .map_err(|e| CreateSuperUserError::RepoError(e.to_string()))?;
                    let ins_res = insert(user).await;
                    match ins_res {
                        Ok(_) => Ok(()),
                        Err(e) => Err(e),
                    }
                }
            }
        }
    }
}

pub enum CreateAccountError {
    RepoError(String),
}

impl ToString for CreateAccountError {
    fn to_string(&self) -> String {
        match self {
            Self::RepoError(es) => es.to_owned(),
        }
    }
}

#[cfg(test)]
mod tests {
    use std::str::FromStr;

    // use futures::{executor::block_on, future::BoxFuture};
    use uuid::Uuid;

    use crate::common::hash_map::hash_map_to_string;

    use super::{
        create_super_user, Account, AccountDto, AccountId, CreateAccountError,
        CreateSuperUserError, User, UserDto,
    };

    fn user_dto() -> UserDto {
        UserDto {
            id: Uuid::from_str("9acd36f9-b9f4-4fd1-840c-c161a9fd3c41").unwrap(),
            username: "someusername".to_string(),
            password: "!Q2w3e4r5t".to_string(),
            roles: "super_user".to_string(),
            account_id: Uuid::from_str("a304f299-b547-4d3d-bd42-732f617b258a").unwrap(),
        }
    }

    fn account_dto() -> AccountDto {
        AccountDto {
            id: Uuid::from_str("3c3f5220-8b3d-40a3-8da2-196a69beaca8").unwrap(),
            // id: "3c3f5220-8b3d-40a3-8da2-196a69beaca8".to_string(),
            name: "edb".to_string(),
        }
    }

    /*
    fn find_existing_super_user<'a>(
        count: &'a mut u8,
    ) -> impl FnOnce() -> BoxFuture<'a, Result<Option<User>, CreateSuperUserError>> {
        move || {
            *count += 1;
            Box::pin(async move { Ok(None) })
        }
    }

    fn insert_user_mock<'a>(
        count: &'a mut u8,
    ) -> impl FnOnce(User) -> BoxFuture<'a, Result<(), CreateSuperUserError>> {
        move |_: User| {
            *count += 1;
            Box::pin(async move { Ok(()) })
        }
    }

    fn find_account_by_id<'a>(
        count: &'a mut u8,
    ) -> impl FnOnce(AccountId) -> BoxFuture<'a, Result<Option<Account>, CreateAccountError>> {
        move |_: AccountId| {
            *count += 1;
            Box::pin(async move { Ok(None) })
        }
    }

    fn fake_account() -> Account {
        Account {
            id: AccountId(Uuid::from_str("ac41d7b5-248c-415c-8728-9cb3bd91a6fb").unwrap()),
            name: "fake".to_string(),
        }
    }

    fn find_account_by_id_mock_found<'a>(
        count: &'a mut u8,
    ) -> impl FnOnce(AccountId) -> BoxFuture<'a, Result<Option<Account>, CreateAccountError>> {
        move |_: AccountId| {
            *count += 1;
            Box::pin(async move { Ok(Some(fake_account())) })
        }
    }

    fn insert_account_mock<'a>(
        count: &'a mut u8,
    ) -> impl FnOnce(Account) -> BoxFuture<'a, Result<(), CreateAccountError>> {
        move |_: Account| {
            *count += 1;
            Box::pin(async move { Ok(()) })
        }
    }

    #[test]
    pub fn test_create_ok() {
        let mut find_existing_super_user_count: u8 = 0;
        let mut insert_count: u8 = 0;
        let mut find_account_by_id_count: u8 = 0;
        let mut insert_account_count: u8 = 0;

        let res = block_on(create_super_user(
            find_existing_super_user(&mut find_existing_super_user_count),
            insert_user_mock(&mut insert_count),
            insert_account_mock(&mut insert_account_count),
            find_account_by_id(&mut find_account_by_id_count),
            &user_dto(),
            &account_dto(),
        ));
        match res {
            Ok(_) => {
                assert_eq!(1, find_existing_super_user_count);
                assert_eq!(1, insert_count);
                assert_eq!(1, find_account_by_id_count);
                assert_eq!(1, insert_account_count);
            }
            Err(_e) => assert!(false, "Super user creation failed"),
        }
    }

    #[test]
    pub fn test_create_super_user_fails_with_invalid_user() {
        let dto = UserDto {
            username: "".to_string(),
            password: "".to_string(),
            ..user_dto()
        };
        let mut find_su_count: u8 = 0;
        let mut insert_count: u8 = 0;
        let mut insert_account_count: u8 = 0;
        let mut find_account_by_id_count: u8 = 0;
        let res = block_on(create_super_user(
            find_existing_super_user(&mut find_su_count),
            insert_user_mock(&mut insert_count),
            insert_account_mock(&mut insert_account_count),
            find_account_by_id(&mut find_account_by_id_count),
            &dto,
            &account_dto(),
        ));
        match res {
            Ok(_) => assert!(false, "Ok encountered where Err expected"),
            Err(e) => match e {
                CreateSuperUserError::UserInvalid(map) => {
                    println!("{}", hash_map_to_string(map));
                    assert_eq!(0, find_su_count);
                    assert_eq!(0, insert_count);
                    assert_eq!(0, find_account_by_id_count);
                    assert_eq!(0, insert_account_count);
                }
                _ => assert!(false, "Incorrect variant found"),
            },
        }
    }

    #[test]
    pub fn test_create_super_user_fails_with_account_found() {
        let mut find_su_count: u8 = 0;
        let mut insert_count: u8 = 0;
        let mut insert_account_count: u8 = 0;
        let mut find_account_by_id_count: u8 = 0;
        let res = block_on(create_super_user(
            find_existing_super_user(&mut find_su_count),
            insert_user_mock(&mut insert_count),
            insert_account_mock(&mut insert_account_count),
            find_account_by_id_mock_found(&mut find_account_by_id_count),
            &user_dto(),
            &account_dto(),
        ));
        match res {
            Ok(_) => assert!(false, "Ok encountered where Err expected"),
            Err(e) => match e {
                CreateSuperUserError::AccountExists => {
                    assert!(true, "Account Exists error encountered")
                }
                _ => assert!(false, "Incorrect variant found"),
            },
        }
    }

    #[test]
    pub fn test_create_super_user_fails_with_account_insert_error() {
        let mut find_su_count: u8 = 0;
        let mut insert_count: u8 = 0;
        let mut insert_account_count: u8 = 0;
        let mut find_account_by_id_count: u8 = 0;

        let insert_account_error = |_: Account| {
            insert_account_count += 1;
            async { Err(CreateAccountError::RepoError("Repo Error".to_string())) }
        };
        let res = block_on(create_super_user(
            find_existing_super_user(&mut find_su_count),
            insert_user_mock(&mut insert_count),
            insert_account_error,
            find_account_by_id(&mut find_account_by_id_count),
            &user_dto(),
            &account_dto(),
        ));
        match res {
            Ok(_) => assert!(false, "Ok encountered where Err expected"),
            Err(e) => match e {
                CreateSuperUserError::RepoError(e) => assert_eq!("Repo Error".to_string(), e),
                _ => assert!(false, "Incorrect variant found"),
            },
        }
    }
    */
}
