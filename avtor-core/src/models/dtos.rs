pub struct LoginDto {
    pub csrf: String,
    pub username: String,
    pub password: String,
}

pub struct SignUpDto {
    pub csrf: String,
    pub username: String,
    pub password: String,
    pub confirm: String,
}
