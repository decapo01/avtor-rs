use chrono::{DateTime, Utc};
use uuid::Uuid;

use super::{invitations::Invitation, login_attempts::LoginAttemptError};

pub struct AccountViewModel {
    pub id: Uuid,
    pub name: String,
    pub users: i32,
    pub invitations: i32,
}

pub struct UserViewModel {
    pub id: Uuid,
    pub username: String,
    pub account_id: Uuid,
    pub account_name: String,
    pub last_login: Option<DateTime<Utc>>,
}

pub struct InvitationViewModel {
    pub id: Uuid,
    pub email: String,
    pub account_id: Uuid,
    pub account_name: String,
    pub invited_on: Option<DateTime<Utc>>,
    pub expiration_date: DateTime<Utc>,
}
