use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Serialize, Deserialize, Clone)]
pub struct BlockedIp {
    pub id: Uuid,
    pub ip: String,
}
