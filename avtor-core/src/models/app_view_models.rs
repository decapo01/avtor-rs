use uuid::Uuid;

pub struct AppViewModel {
    pub id: Uuid,
    pub name: String,
    pub sign_in_redirect: String,
    pub sign_up_redirect: String,
    pub accounts: i64,
    pub users: i64,
    pub invites: i64,
}
