use serde::{Deserialize, Serialize};
use uuid::Uuid;
use validator::Validate;

#[derive(Serialize, Deserialize, Clone)]
pub struct AppId(Uuid);

#[derive(Serialize, Deserialize, Clone)]
pub struct App {
    pub id: AppId,
    pub name: String,
    pub sign_in_redirect: String,
    pub sign_up_redirect: String,
}

#[derive(Serialize, Deserialize, Clone, Validate)]
pub struct AppDto {
    pub id: Uuid,
    #[validate(length(min = 1, max = 255))]
    pub name: String,
    #[validate(url)]
    pub sign_in_redirect: String,
    #[validate(url)]
    pub sign_up_redirect: String,
}

impl From<AppDto> for App {
    fn from(value: AppDto) -> Self {
        App {
            id: AppId(value.id),
            name: value.name,
            sign_in_redirect: value.sign_in_redirect,
            sign_up_redirect: value.sign_up_redirect,
        }
    }
}
