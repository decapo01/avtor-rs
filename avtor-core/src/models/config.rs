use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Serialize, Deserialize, Clone)]
pub struct Config {
    pub id: Uuid,
    pub allow_auto_registration: bool,
    pub allow_external_invitations: bool,
    pub allow_login_attempts: u32,
    pub super_user_id: Uuid,
    pub super_user_password: String,
    pub super_user_username: String,
    pub main_account_id: Uuid,
    pub main_account_name: String,
}
