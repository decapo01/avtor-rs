use avtor_core::models::accounts::{Account, AccountId};
use yew::{prelude::*, virtual_dom::VNode};
use yew_router::prelude::Link;

use crate::{
    components::{navbar::NavBar, table::Table},
    routes::Route,
};

#[derive(Properties, Clone, PartialEq)]
pub struct Props {}

#[function_component]
pub fn List(_props: &Props) -> Html {
    let accounts: Vec<Account> = vec![
        Account {
            id: AccountId(uuid::Uuid::new_v4()),
            name: "Account 1".to_string(),
        },
        Account {
            id: AccountId(uuid::Uuid::new_v4()),
            name: "Account 2".to_string(),
        },
    ];
    let cols = vec!["Id".to_string(), "Name".to_string(), "".to_string()];
    let other_render_row: Callback<Account, VNode> = Callback::from(|a: Account| {
        html! {
            <>
                <td class="border p-3">{ a.id.0.to_string() }</td>
                <td class="border p-3">{ a.name }</td>
                <td class="border p-3">
                    <Link<Route> to={Route::Account { id: a.id.0.to_string() }} classes="btn btn-blue">{ "Edit" }</Link<Route>>
                </td>
            </>
        }
    });
    html! {
        <div class="container mx-12 my-5">
            <NavBar />
            <h1 class="text-2xl font-semibold my-5">{ "Accounts" }</h1>
            <Link<Route> to={Route::AccountNew} classes="btn btn-blue my-5 shadow-lg">{ "New" }</Link<Route>>
            <Table<Account> cols={cols} render_row={other_render_row} items={accounts} />
        </div>
    }
}
