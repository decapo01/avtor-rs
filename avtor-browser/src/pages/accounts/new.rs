use yew::prelude::*;

use crate::{components::navbar::NavBar, pages::accounts::form::Form};

#[derive(Properties, Clone, PartialEq)]
pub struct Props {}

#[function_component]
pub fn NewAccount(_props: &Props) -> Html {
    html! {
        <div class="container mx-12 my-5">
            <NavBar />
            <h1 class="text-2xl font-semibold my-5">{ "Accounts" }</h1>
            // <Form is_edit={false} />
        </div>
    }
}
