use avtor_core::models::accounts::Account;
use gloo::{console::log, net::http::Request};
use web_sys::HtmlInputElement;
use yew::prelude::*;

#[derive(Clone, PartialEq, Properties)]
pub struct Props {
    pub is_edit: bool,
    pub name: String,
}

pub async fn create_account(_account: &Account) -> Result<Account, String> {
    let req = Request::post("/api/accounts")
        .header("x-auth-token", "")
        .header("content-type", "json")
        .body("")
        .send()
        .await
        .unwrap();
    if req.ok() {
        let a = req.json::<Account>().await.unwrap();
        Ok(a)
    } else {
        Err("bad".to_string())
    }
}

#[function_component]
pub fn Form(props: &Props) -> Html {
    let account = use_state(Account::default);
    let name_ref = use_node_ref();
    let handle_submit = {
        let inner_name_ref = name_ref.clone();
        Callback::from(move |event: SubmitEvent| {
            event.prevent_default();
            let n = inner_name_ref.cast::<HtmlInputElement>().unwrap().value();
            log!("button clicked {}", n);
        })
    };

    let handle_change = Callback::from(|e: Event| {
        let val_opt = e.target();
        // let val_du = e.target_dyn_into::<String>();
        match val_opt {
            // Some(val) => account.set(Account {
            //     name: val.to_string(),
            //     ..*account
            // }),
            _ => log!("nothing"),
        }
    });

    // let btn_val = if props.isEdit { "Edit" } else { "Create" };
    html! {
        <form onsubmit={handle_submit}>
            <div class="mb-4">
                <label class="block">{"Name"}</label>
                <input
                    class="border-2 rounded"
                    name={ props.name.clone() }
                    // ref={ props.name_ref.clone() }
                />
            </div>
            <div class="mb-6">
                // <button class="btn btn-blue shadow-lg" value={ btn_val }>{ btn_val }</button>
                <h1>{"hello"}</h1>
            </div>
        </form>
    }
}
