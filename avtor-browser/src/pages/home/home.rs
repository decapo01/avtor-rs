use crate::components::navbar::NavBar;
use avtor_core::models::accounts::{Account, AccountId};
use yew::prelude::*;

#[function_component]
pub fn Home() -> Html {
    let account = Account {
        id: AccountId(uuid::Uuid::new_v4()),
        name: "Account 1".to_string(),
    };
    let counter = use_state(|| 0);
    let onclick = {
        let counter = counter.clone();
        move |_| {
            let value = *counter + 1;
            counter.set(value);
        }
    };

    html! {
        <div class="container mx-12 my-5">
            <NavBar />
            <h1> {account.name} </h1>
            <h1> {account.id.0} </h1>
            <button class="btn btn-blue" {onclick}>{ "+1" }</button>
            <p>{ *counter }</p>
        </div>
    }
}
