use avtor_core::models::accounts::{Account, AccountId};
use yew::prelude::*;

pub mod common;
pub mod components;
pub mod pages;
pub mod routes;
pub mod services;

use crate::routes::Main;

#[function_component]
fn App() -> Html {
    let account = Account {
        id: AccountId(uuid::Uuid::new_v4()),
        name: "Account 1".to_string(),
    };
    let counter = use_state(|| 0);
    let onclick = {
        let counter = counter.clone();
        move |_| {
            let value = *counter + 1;
            counter.set(value);
        }
    };

    html! {
        <div class="container mx-12 my-5">
            <h1> {account.name} </h1>
            <h1> {account.id.0} </h1>
            <button class="btn btn-blue" {onclick}>{ "+1" }</button>
            <p>{ *counter }</p>
        </div>
    }
}

fn main() {
    // yew::Renderer::<App>::new().render();
    yew::Renderer::<Main>::new().render();
}
