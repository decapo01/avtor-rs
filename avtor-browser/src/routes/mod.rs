use yew::prelude::*;
use yew_router::prelude::*;

use crate::pages::accounts::edit::Edit as AccountEdit;
use crate::pages::accounts::list::List as AccountList;
use crate::pages::accounts::new::NewAccount;
use crate::pages::home::home::Home;

#[derive(Clone, Routable, PartialEq)]
pub enum Route {
    #[at("/")]
    Home,
    #[at("/accounts")]
    Accounts,
    #[at("/accounts/:id")]
    Account { id: String },
    #[at("/accounts/new")]
    AccountNew,
    #[at("/users")]
    Users,
    #[at("/users/:id")]
    User { id: String },
    #[at("/invitations")]
    Invitations,
    #[at("/invitations/:id")]
    Invitation { id: String },
    #[at("/settings")]
    Settings,
    #[not_found]
    #[at("/404")]
    NotFound,
}

pub fn switch(route: Route) -> Html {
    match route {
        Route::Home => html! { <Home /> },
        Route::Accounts => html! { <AccountList /> },
        Route::Account { id } => html! { <AccountEdit /> },
        Route::AccountNew => html! { <NewAccount /> },
        Route::Users => todo!(),
        Route::User { id } => todo!(),
        Route::Invitations => todo!(),
        Route::Invitation { id } => todo!(),
        Route::Settings => todo!(),
        Route::NotFound => todo!(),
    }
}

#[function_component(Main)]
pub fn app() -> Html {
    html! {
        <BrowserRouter>
            <Switch<Route> render={switch} />
        </BrowserRouter>
    }
}
