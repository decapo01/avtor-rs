use yew::prelude::*;
use yew_router::prelude::Link;

use crate::routes::Route;

#[function_component]
pub fn NavBar() -> Html {
    html! {
        <nav class="relative container mx-auto">
            <div class="flex items-center justify-between">
                <div>
                    <Link<Route> to={Route::Home}>{ "Avtor" }</Link<Route>>
                    // <a href="#" class="nav-link">{ "Avtor" }</a>
                </div>
                <ul class="flex items-center">
                    <li>
                        <Link<Route> to={Route::Accounts} classes="nav-link">{ "Accounts" }</Link<Route>>
                    </li>
                    <li>
                        <Link<Route> to={Route::Users} classes="nav-link">{ "Users" }</Link<Route>>
                    </li>
                    <li>
                        <Link<Route> to={Route::Invitations} classes="nav-link">{ "Invitations" }</Link<Route>>
                    </li>
                    <li>
                        <Link<Route> to={Route::Settings} classes="nav-link">{ "Settings" }</Link<Route>>
                    </li>
                </ul>
            </div>
        </nav>
    }
}
