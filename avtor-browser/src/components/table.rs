use std::ops::Deref;

use yew::prelude::*;

#[derive(Clone, PartialEq, Properties)]
pub struct Props<T: Clone + PartialEq> {
    pub cols: Vec<String>,
    pub render_row: Callback<T, Html>,
    pub items: Vec<T>,
}

#[function_component]
pub fn Table<T: Clone + PartialEq>(props: &Props<T>) -> Html {
    let render_row = &props.render_row;
    let cols = &props.cols;
    let items = &props.items;
    html! {
        <table class="my-5 shadow-lg">
            <thead>
                <tr>
                    {for cols.iter().map(|col| {
                            html! {
                                <th class="border"> { format!("{}", col.clone()) } </th>
                            }
                    })}
                </tr>
            </thead>
            <tbody>
                {for items.iter().map(|item| {
                        let row = render_row.emit(item.clone());
                        html! {
                            <tr>
                                { row }
                            </tr>
                        }
                })}
            </tbody>
        </table>
    }
}
